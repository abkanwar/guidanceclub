//
//  RegistrationModuleB.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface RegistrationModuleB : UIViewController <NSURLSessionDelegate>{
    
    __weak IBOutlet UIScrollView        * scrollView;
    __weak IBOutlet UIButton            * buttonLogin;
    
    __weak IBOutlet UITextField         * textFieldFirstName;
    __weak IBOutlet UITextField         * textFieldLastName;
    __weak IBOutlet UITextField         * textFieldCountry;
    __weak IBOutlet UITextField         * textFieldState;
    __weak IBOutlet UITextField         * textFieldCity;
    __weak IBOutlet UITextField         * textFieldAvatarName;
}

@property (nonatomic, strong) NSMutableDictionary * dictUserDetail;

@end
