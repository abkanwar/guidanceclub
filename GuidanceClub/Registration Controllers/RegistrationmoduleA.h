//
//  RegistrationmoduleA.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface RegistrationmoduleA : UIViewController{
    
    
    __weak IBOutlet UITextField     * textFieldEmail;
    __weak IBOutlet UITextField     * textFieldPassword;
    __weak IBOutlet UITextField     * textFieldConfirmPassword;
    __weak IBOutlet UITextField     * textFieldPhone;
    __weak IBOutlet UITextField     * textFieldCode;
    
    __weak IBOutlet UIButton        * buttonLogin;
}

@end
