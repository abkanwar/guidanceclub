//
//  RegistrationModuleB.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "RegistrationModuleB.h"
#import "Utility.h"
#import "SVProgressHUD.h"

@interface RegistrationModuleB ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    
}
@property(nonatomic, strong) UIPickerView   * countryPicker;
@property(nonatomic, strong) NSMutableArray * arrayCountries;
@property(nonatomic, strong) NSMutableArray * arrayStates;
@property(nonatomic, strong) NSString       * countryID;
@property(nonatomic, strong) NSString       * stateID;

@end

@implementation RegistrationModuleB
@synthesize dictUserDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    _arrayCountries             = [[NSMutableArray alloc] init];
    _arrayCountries             = [Utility fetchCountryList];
    
    _arrayStates                = [[NSMutableArray alloc] init];
    
    textFieldCountry.inputView  = self.countryPicker;
    textFieldState.inputView    = self.countryPicker;
    
    _countryID                  = @"0";
    _stateID                    = @"0";
    
    /*NSLocale    * locale              = [NSLocale currentLocale];
    NSString    * countryCode           = [locale objectForKey:NSLocaleCountryCode];
    NSString    * country               = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
    NSArray     * arrayFilteredState    = [_arrayCountries filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", country]];
    NSUInteger   index          = [_arrayCountries indexOfObject:[arrayFilteredState objectAtIndex:0]];
    [_countryPicker selectRow:index inComponent:0 animated:YES];*/
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame), CGRectGetMaxY(buttonLogin.frame));
}

-(IBAction)loginButtonAction:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)registerButtonAction:(id)sender{
    
    [self.view endEditing:YES];
    if([self validateAllFields]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showWithStatus:STATUS_LOADING];
        });
        [dictUserDetail setValue:textFieldFirstName.text forKey:@"firstName"];
        [dictUserDetail setValue:textFieldLastName.text forKey:@"lastName"];
        [dictUserDetail setValue:_countryID forKey:@"country"];
        [dictUserDetail setValue:textFieldCity.text forKey:@"city"];
        [dictUserDetail setValue:textFieldAvatarName.text forKey:@"avatarName"];
        [dictUserDetail setValue:_stateID forKey:@"state"];
        
        NSString    * urlString       = [NSString stringWithFormat:@"%@%@",BASE_URL, REGISTER_URL];
        //NSError     * error;
        //NSData      * requestData     = [NSJSONSerialization  dataWithJSONObject:dictUserDetail options:0 error:&error];
        //'name','avatar_name','avatar_image','email','password','mobile_no','country','state','city','zipcode','status','is_ip'
        
        NSString * firtName     = [dictUserDetail valueForKey:@"firstName"];
        NSString * lastName     = [dictUserDetail valueForKey:@"lastName"];
        
        NSString * avatarName   = [dictUserDetail valueForKey:@"avatarName"];
        NSString * avatarImage  = @"";
        NSString * email        = [dictUserDetail valueForKey:@"email"];
        NSString * password     = [dictUserDetail valueForKey:@"password"];
        NSString * mobileNo     = [dictUserDetail valueForKey:@"phone"];
        NSString * country      = [dictUserDetail valueForKey:@"country"];
        NSString * state        = [dictUserDetail valueForKey:@"state"];
        NSString * city         = [dictUserDetail valueForKey:@"city"];
        NSString * zipCode      = @"";
        NSString * status       = @"";
        NSString * isIP         = @"";
        
        NSMutableString * post        = [NSMutableString stringWithFormat:@"firstname=%@&lastname=%@&avatar_name=%@&avatar_image=%@&email=%@&password=%@&mobile_no=%@&country=%@&state=%@&city=%@&zipcode=%@&status=%@&is_ip=%@&rating=0&users_count=0", firtName, lastName, avatarName, avatarImage, email, password, mobileNo, country, state, city, zipCode, status, isIP];
        NSData          * requestData = [post dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];
        
        NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
            
            if (error) {
                NSLog(@"Error:%@", [error localizedDescription]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [Utility showToastWithMessage:[error localizedDescription] target:self];
                });

                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            NSError      * errorJson    = nil;
            NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
            NSLog(@"Response Dict = %@",responseDict);
            
            
            if(!responseDict && [errorJson code] == 3840){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                    [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
                });
                return;
            }
            
            if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
               
                //NSMutableDictionary * dictDetails = [responseDict valueForKeyPath:@"response.success"];
                //[Utility saveUserDetails:dictDetails];
                dispatch_async(dispatch_get_main_queue(), ^{
                    SHOW_ALERT(APP_NAME, @"You have successfully registered with us and verification email has been sent on your registered email address.", nil, @"OK", nil);
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
            }
            else if([[responseDict valueForKey:@"status_code"] integerValue] == 400){
                
                if([responseDict valueForKeyPath:@"message.errors.avatar_name"]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utility showToastWithMessage:@"Avatar name must be alphanumeric" target:self];
                    });
                    return;
                }
                if([responseDict valueForKeyPath:@"message.errors.Email"]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utility showToastWithMessage:@"Email Id has already been taken." target:self];
                    });
                    return;
                }
                if([responseDict valueForKeyPath:@"message.errors.rating"]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utility showToastWithMessage:@"Rating is invalid." target:self];
                    });
                    return;
                }
                if([responseDict valueForKeyPath:@"message.errors.mobile_no"]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utility showToastWithMessage:@"Mobile Number has already been taken." target:self];
                    });
                    return;
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utility showToastWithMessage:@"An error encountered while registration please try again!" target:self];
                    });

            }
            
        }
        }];
        [postDataTask resume];
    }
    
    
//    if([[responseDict valueForKey:@"status_code"] integerValue] == 400){
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            NSString * message = @"An error encountered while registration please try again!";
//            if([[[responseDict valueForKey:@"message"] valueForKey:@"errors"] valueForKey:@"mobile_no"])
//                message = [[[responseDict valueForKey:@"message"] valueForKey:@"errors"] valueForKey:@"mobile_no"];
//            [Utility showToastWithMessage:[[[[responseDict valueForKey:@"message"] valueForKey:@"errors"] valueForKey:@"mobile_no"] objectAtIndex:0] target:self];
//        });
//        
//    }
}

-(BOOL)validateAllFields{
    
    BOOL isValid = NO;
    
    if(![[textFieldFirstName text] length]){
        SHOW_ALERT(APP_NAME, @"Please enter first", nil, @"OK", nil);
        return isValid;
    }
    if(![[textFieldLastName text] length]){
        SHOW_ALERT(APP_NAME, @"Please enter last name", nil, @"OK", nil);
        return isValid;
    }
    
    if(![[textFieldCountry text] length]){
        SHOW_ALERT(APP_NAME, @"Please select country", nil, @"OK", nil);
        return isValid;
    }
    
    //if(![[textFieldCity text] length]){
    //    SHOW_ALERT(APP_NAME, @"Please enter city", nil, @"OK", nil);
    //    return isValid;
    //}
    
    if(![[textFieldAvatarName text] length]){
        SHOW_ALERT(APP_NAME, @"Please select avatar name", nil, @"OK", nil);
        return isValid;
    }
    
//    NSString        * strTemp       = [textFieldAvatarName text];
//    NSCharacterSet  * alphaSet      = [NSCharacterSet alphanumericCharacterSet];
//    BOOL              valid         = [[strTemp stringByTrimmingCharactersInSet:alphaSet] isEqualToString:@""];
//    if(!valid){
//        SHOW_ALERT(APP_NAME, @"Avatar name must contains alphanumeric characters only", nil, @"OK", nil);
//        return isValid;
//    }
    
    
    
    
    return YES;
}

- (UIPickerView *)countryPicker {
    
    if (_countryPicker == nil) {
        
        self.countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame), 216)];
        _countryPicker.delegate = self;
    }
    return _countryPicker;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
   
    if([textFieldCountry isFirstResponder])
        return [_arrayCountries count];
    else
        return [_arrayStates count];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([textFieldCountry isFirstResponder])
        return [[_arrayCountries objectAtIndex:row] valueForKey:@"name"];
    else
        return [[_arrayStates objectAtIndex:row] valueForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if([textFieldCountry isFirstResponder]){
        
        textFieldCountry.text = [[_arrayCountries objectAtIndex:row] valueForKey:@"name"];
        
        textFieldState.text   = @"";
        _stateID              = @"0";

        _countryID            = [[_arrayCountries objectAtIndex:row] valueForKey:@"countryId"];
        _arrayStates          = [Utility fetchStateListWithCoutryID:[[_arrayCountries objectAtIndex:row] valueForKey:@"countryId"]];

    }else{
        if([_arrayStates count]){
            textFieldState.text = [[_arrayStates objectAtIndex:row] valueForKey:@"name"];
            _stateID            = [[_arrayStates objectAtIndex:row] valueForKey:@"stateId"];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    /*if(textField == textFieldCountry && [textFieldCountry isFirstResponder]){
     
     NSLocale    * locale                = [NSLocale currentLocale];
     NSString    * countryCode           = [locale objectForKey:NSLocaleCountryCode];
     NSString    * country               = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
     NSArray     * arrayFilteredState    = [_arrayCountries filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", country]];
     NSUInteger   index          = [_arrayCountries indexOfObject:[arrayFilteredState objectAtIndex:0]];
     [_countryPicker selectRow:index inComponent:0 animated:YES];
     }*/
    
    /*
     if(textField == textFieldState && [textFieldState isFirstResponder] && ![[textFieldCountry text] length]){
     
     //[Utility showToastWithMessage:@"Please select country" target:self];
     SHOW_ALERT(APP_NAME, @"Please select country first to choose state", self, @"OK", nil);
     [self reloadPickerView];
     return NO;
     }
     
     if(textField == textFieldState && [textFieldState isFirstResponder] && [[textFieldCountry text] length]){
     
     if(![_arrayStates count]){
     //[Utility showToastWithMessage:@"Please select country" target:self];
     SHOW_ALERT(APP_NAME, @"No State to select", self, @"OK", nil);
     [self reloadPickerView];
     return NO;
     }else{
     [self reloadPickerView];
     }
     }
     if(textField == textFieldCountry && [textFieldCountry isFirstResponder]){
     [self reloadPickerView];
     }*/
    [self reloadPickerView];
    return YES;
}


-(void)reloadPickerView{
    [_countryPicker reloadAllComponents];
    [_countryPicker selectRow:0 inComponent:0 animated:YES];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    [self.view endEditing:YES];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
