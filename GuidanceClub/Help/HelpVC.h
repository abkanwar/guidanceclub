//
//  HelpVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "RevealController.h"
#import "ZUUIRevealController.h"

@interface HelpVC : UIViewController<ZUUIRevealControllerDelegate>

@end
