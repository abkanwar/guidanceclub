//
//  MyAppointmants.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "MyAppointmants.h"

@interface MyAppointmants ()

@end

@implementation MyAppointmants

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.title = @"My Appointmants";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:211/255.0 green:94/255.0 blue:62/255.0 alpha:1.0f]}];
    
    UIBarButtonItem * itemLeftA          = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    NSArray         * arrayLeftBatrItems = [[NSArray alloc]initWithObjects:itemLeftA, nil];
    self.navigationItem.leftBarButtonItems = arrayLeftBatrItems;
    
    //[[APP_DELEGATE  revealViewController] setDelegate:self];
    [APP_DELEGATE revealViewController].enableSwipeAndTapGestures = YES;
    
    

}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    UIImageView * back = [[UIImageView alloc]initWithFrame:self.view.frame];
//    back.image          = [UIImage imageNamed:@"Appointments"];
//    [self.view addSubview:back];
}

//- (void)revealController:(ZUUIRevealController *)revealController willRevealRearViewController:(UIViewController *)rearViewController{
//    
//    UIView * view               = [[UIView alloc] initWithFrame:self.view.window.frame];
//    view.backgroundColor        = [UIColor blackColor];
//    view.tag                    = 555;
//    view.userInteractionEnabled = YES;
//    [self.view addSubview:view];
//    view.alpha                  = 0.f;
//    
//    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
//        view.alpha                  = 0.35f;
//    }
//                     completion:^(BOOL finished) {
//                         
//                     }];
//    //self.view.userInteractionEnabled = NO;
//}
//- (void)revealController:(ZUUIRevealController *)revealController willHideRearViewController:(UIViewController *)rearViewController{
//    
//    UIView * view = [self.view viewWithTag:555];
//    [view removeFromSuperview];
//    //self.view.userInteractionEnabled = YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
