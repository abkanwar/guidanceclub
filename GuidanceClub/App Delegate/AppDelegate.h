//
//  AppDelegate.h
//  GuidanceClub
//
//  Created by Raman Kant on 12/15/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "BasicPhone.h"
#import "RearTableVC.h"
#import "RevealController.h"
#import "HomeViewController.h"
#import "MainViewController.h"
#import "BasicPhoneNotifications.h"

#import "Utility.h"
#import "SharedClass.h"
#import "ViewCall.h"
#import "InvoiceView.h"
#import "SVProgressHUD.h"

#import <MZTimerLabel.h>

#import <PushKit/PushKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLSessionDelegate>{//, TCDeviceDelegate, TCConnectionDelegate, PKPushRegistryDelegate
    
     UIAlertView                                                        * alertViewCall;
}

@property (strong, nonatomic)           UIWindow                        * window;

@property (readonly, strong, nonatomic) NSManagedObjectContext          * managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel            * managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator    * persistentStoreCoordinator;

@property (nonatomic, strong)           RevealController                * revealViewController;
@property (strong, nonatomic)           UINavigationController          * mainNavigationController;


@property (nonatomic, strong, getter = phone) BasicPhone                * phone;
@property (readwrite)                         BOOL                        speakerEnable;
@property (readwrite)                         BOOL                        muteEnable;

@property (nonatomic, strong)           ViewCall                        * viewCall;

@property (strong)                      Reachability                    * internetConnectionReach;


- (void)saveContext;
- (NSURL*)applicationDocumentsDirectory;
- (BOOL)isForeground;

- (void)changeRootControllerLogin;
- (void)changeRootControllerToRevealController;

//- (void)login;
//- (void)call;
//- (void)registerTCDeviceWithToken;
//-(void)connect:(NSDictionary*)dictParameters;

- (void)addCallViewToWindow:(NSMutableDictionary*)dictUserDetails;


@end

