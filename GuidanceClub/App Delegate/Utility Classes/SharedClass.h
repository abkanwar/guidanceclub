//
//  SharedClass.h
//  PruPms
//
//  Created by Raman Kant on 2/26/15.
//  Copyright (c) 2015 Raman Kant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface SharedClass : NSObject{
    
    //NSMutableDictionary                             * dictUserProfileDetails;
}
//@property (nonatomic, strong) NSString              * clientIDToCall;
//@property (nonatomic, strong) NSMutableDictionary   * dictUserProfileDetails;

@property (readwrite) BOOL                            isNetwork;

+(SharedClass *)sharedInstance;

// +(SharedClass*)singletonInstance; //
// AppDelegate * delegate ();        //


@end
