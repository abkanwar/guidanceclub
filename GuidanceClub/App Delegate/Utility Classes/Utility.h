//
//  Utility.h
//  Rarelon
//
//  Created by Raman Kant on 11/30/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"
#include "Constants.h"

@interface Utility : NSObject



+ (BOOL)is_IP;
+ (NSString *)appSID;
+ (NSString *)userID;
+ (NSString *)clientID;
+ (NSString *)deviceType;
+ (NSString *)deviceToken;
+ (NSString *)mobileNumber;
+ (NSString *)capabilityToken;

+(NSString*)timeFromTimeStamp:(NSString*)timeStamp;

//+ (void)showAlertWithMessage:(NSString *)message;
+ (BOOL)ValidateEmailAddress:(NSString *)checkString;
+ (UIBarButtonItem *)addRevealBarButtonWithAction:(SEL)action target:(id)sender;
+ (void)showToastWithMessage:(NSString *)msg target:(UIViewController *)target;

+(NSMutableDictionary*)fetchUserDetails;
+(void)saveUserDetails:(NSMutableDictionary*)userDetails;

+(NSMutableArray*)fetchCountryList;
+(NSMutableArray*)fetchStateListWithCoutryID:(NSString*)countrtId;

+(void)saveLastCallInfo:(NSMutableDictionary*)info;
+(NSMutableDictionary*)fetchLastCallInfo;
+(void)deleteLastCallInfo;

+(BOOL)validateString:(NSString*)value;

+(NSString*)createCallDurationString:(float)duration;

@end
