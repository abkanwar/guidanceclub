//
//  Utility.m
//  Rarelon
//
//  Created by Raman Kant on 11/30/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import "Utility.h"


@implementation Utility

+ (UIBarButtonItem *)addRevealBarButtonWithAction:(SEL)action target:(id)sender {
    
    //UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithImage:nil landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:sender action:action];
    
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"[]" style:UIBarButtonItemStylePlain target:sender action:action];
    
    return buttonItem;
}

+ (BOOL)ValidateEmailAddress:(NSString *)checkString {
    
    BOOL stricterFilter     = NO;
    NSString * stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString * laxString    = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString * emailRegex   = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest  = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (NSString *)deviceToken {
    return [[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_TOKEN];
}

//+ (NSString *)userID {
//    return [[NSUserDefaults standardUserDefaults] valueForKey:USER_ID];
//}

+ (NSString *)deviceType {
    return [[[UIDevice currentDevice] systemName] stringByAppendingFormat:@" %@", [[UIDevice currentDevice] systemVersion]];
}

+ (void)showToastWithMessage:(NSString *)msg target:(UIViewController *)target {
    
    NSLog(@"TOAST Message: %@", msg);
    MBProgressHUD * hud     = [MBProgressHUD showHUDAddedTo:target.view.window animated:YES];
    // Configure for text only and offset down
    hud.mode                = MBProgressHUDModeText;
    hud.labelText           = @"";
    hud.detailsLabelText    = msg;
    hud.margin              = 10.f;
    hud.yOffset             = 150.f;
    hud.labelFont           = [UIFont boldSystemFontOfSize:14.0f];
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:2];
}

/*+(void)showAlertWithMessage:(NSString *)message{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        SHOW_ALERT(APP_NAME, message, nil, @"OK", nil);
    });
}*/

+(void)saveUserDetails:(NSMutableDictionary*)userDetails{
    [[NSUserDefaults standardUserDefaults] setObject:userDetails forKey:@"userDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)userID{
    
    NSMutableDictionary * dictUserDetails =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    return [dictUserDetails valueForKey:@"_id"];
}

+ (NSString *)appSID{
    
    NSMutableDictionary * dictUserDetails =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    return [dictUserDetails valueForKeyPath:@"twilio.appsid"];
}

+ (NSString *)clientID{
    
    NSMutableDictionary * dictUserDetails =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    return [dictUserDetails valueForKeyPath:@"avatar.name"];
}

+ (NSString *)mobileNumber{
    
    NSMutableDictionary * dictUserDetails =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    return [dictUserDetails valueForKeyPath:@"mobile_no"];
}

+ (NSString *)capabilityToken{
    
    NSMutableDictionary * dictUserDetails =  [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
    return [dictUserDetails valueForKeyPath:@"twilio.callToken"];
}

+ (BOOL)is_IP{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"is_IP"];
}

+(NSMutableDictionary*)fetchUserDetails{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"userDetails"];
}

+(NSMutableArray*)fetchCountryList{
    
    NSString        * filePath  = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"];
    NSData          * data      = [NSData dataWithContentsOfFile:filePath];
    NSError         * error     = nil;
    NSMutableArray  * arrayCountries         = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%s: JSONObjectWithData error: %@", __FUNCTION__, error);
    return arrayCountries;
}

+(NSMutableArray*)fetchStateListWithCoutryID:(NSString*)countrtId{
    
    NSString        * filePath      = [[NSBundle mainBundle] pathForResource:@"states" ofType:@"json"];
    NSData          * data          = [NSData dataWithContentsOfFile:filePath];
    NSError         * error         = nil;
    NSMutableArray  * arrayStates   = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"%s: JSONObjectWithData error: %@", __FUNCTION__, error);
    
    NSArray         * arrayFilteredState = [arrayStates filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(countryId == %@)", countrtId]];
    return [arrayFilteredState mutableCopy];
}

+(NSString*)timeFromTimeStamp:(NSString*)timeStamp{
    
    NSTimeInterval    timeInterval      = [timeStamp doubleValue];
    NSDate          * date              = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateFormatter * formatter         = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSString        * timeString        = [formatter stringFromDate:date];
    return timeString;
}

+(void)saveLastCallInfo:(NSMutableDictionary*)info{
    [[NSUserDefaults standardUserDefaults] setObject:info forKey:@"call_info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSMutableDictionary*)fetchLastCallInfo{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"call_info"] mutableCopy];
}

+(void)deleteLastCallInfo{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"call_info"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)validateString:(NSString*)value{
    if(![value isEqual:[NSNull null]] && ![value isEqual:@""] && ![value isEqual:@"<null>"])
        return YES;
    else
        return NO;
        
}


+(NSString*)createCallDurationString:(float)duration{
    
    int totalSeconds    = duration;
    int seconds         = totalSeconds % 60;
    int minutes         = (totalSeconds / 60) % 60;
    int hours           = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}


@end
