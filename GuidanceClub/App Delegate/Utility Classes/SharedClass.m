//
//  SharedClass.m
//  PruPms
//
//  Created by Raman Kant on 2/26/15.
//  Copyright (c) 2015 Raman Kant. All rights reserved.
//

#import "SharedClass.h"
#import "Constants.h"
#import "AppDelegate.h"


@implementation SharedClass
//@synthesize dictUserProfileDetails, clientIDToCall;
@synthesize isNetwork;

static SharedClass * _singleton = nil;

#pragma mark - Singleton Object -

+(SharedClass *)sharedInstance{
    
    @synchronized([SharedClass class]){
        if (!_singleton)
            _singleton = [[self alloc] init];
        return _singleton;
    }
    return nil;
}

/*+(SharedClass*)singletonInstance{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _singleton = [[self alloc] init];
    });
    return _singleton;

}*/


//AppDelegate * delegate (){
//    return (AppDelegate*)[[UIApplication sharedApplication]delegate];
//}

void showAlert (NSString * title, NSString * message){
}

@end
