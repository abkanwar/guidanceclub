//
//  Constants.h
//  PruPms
//
//  Created by Raman Kant on 2/26/15.
//  Copyright (c) 2015 Raman Kant. All rights reserved.
//

#ifndef PruPms_Constants_h
#define PruPms_Constants_h

#import "AppDelegate.h"

// Alert Macro //
#define SHOW_ALERT(title,msg,del,cancel,other) \
do { \
UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:del cancelButtonTitle:cancel otherButtonTitles:other,nil]; \
[_alert show]; \
} while(0);


#define APP_NAME                        @"Guidance Club"

#define USER_ID                         @"UserID"
#define LOGGED_IN                       @"LoggedIn"
#define DEVICE_TOKEN                    @"DeviceToken"
#define PROFILE_PICTURE                 @"ProfilePicture"
#define STATUS_LOADING                  @"Please wait..."

#define CAPABILITY_TOKEN                @"CapabilityToken"

#define ERROR_MESSAGE                   @"An error encountered \nPlease try again!"


#define APP_DELEGATE                    (AppDelegate*)[[UIApplication sharedApplication] delegate]
#define manageObjContext                [(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext]
#define VCWithIdentifier(identifier)    [Storyboard instantiateViewControllerWithIdentifier:identifier]

#define iPhone4                         [[UIScreen mainScreen] bounds].size.height == 480.0f

#define BASE_URL                        @"http://52.27.166.47/frontend/web/index.php?r=webservice/"
#define REGISTER_URL                    @"users/register"
#define LOGIN_URL                       @"users/login"
#define BECOME_IP                       @"users/update"
#define FORGOTPASSWORD_URL              @""
#define SEARCH                          @"search"
#define USER_PROFILE                    @"users/profile"
#define USER_PAYMENT                    @"calls/history"
#define CALL_INVOICE                    @"calls/last-invoice" //calls/invoice

#define HEROKU_LIVE                     @"https://guidanceclub.herokuapp.com/"
#define HEROKU_LOCAL                    @"http://52.27.166.47:5000/"


#define COMMON_COLOR                    [UIColor colorWithRed:205/255.0f green:96/255.0f blue:57/255.0f alpha:1.0f]



 /*  #if TARGET_IPHONE_SIMULATOR
  *  #else
  *  #endif
  */

/* Web Services Url's */




#endif
