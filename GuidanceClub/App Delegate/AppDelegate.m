//
//  AppDelegate.m
//  GuidanceClub
//
//  Created by Raman Kant on 12/15/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import "AppDelegate.h"
//#import "SSKeychain.h"
#import <IQKeyboardManager.h>



@interface AppDelegate (){
    
}

// Notifications //
-(void)loginDidStart:(NSNotification*)notification;
-(void)loginDidFinish:(NSNotification*)notification;
-(void)loginDidFailWithError:(NSNotification*)notification;

-(void)connectionDidConnect:(NSNotification*)notification;
-(void)connectionDidFailToConnect:(NSNotification*)notification;
-(void)connectionIsDisconnecting:(NSNotification*)notification;
-(void)connectionDidDisconnect:(NSNotification*)notification;
-(void)connectionDidFailWithError:(NSNotification*)notification;

-(void)pendingIncomingConnectionDidDisconnect:(NSNotification*)notification;
-(void)pendingIncomingConnectionReceived:(NSNotification*)notification;

-(void)deviceDidStartListeningForIncomingConnections:(NSNotification*)notification;
-(void)deviceDidStopListeningForIncomingConnections:(NSNotification*)notification;
// Notifications //

@end



@implementation AppDelegate
@synthesize phone = _phone;
@synthesize internetConnectionReach;
//@synthesize phoneDevice;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    _phone = [self phone];
    
    //NSString * string = [self getUniqueDeviceIdentifierAsString];
    //NSLog(@"Unique Id of App = %@",string);
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [self changeStatusBarColor];
    
    // Register for remote notifications //
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        // use registerUserNotificationSettings
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
    }
    else {
        // use registerForRemoteNotificationTypes: //
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }

    
    //BOOL scheduled = [[UIApplication sharedApplication] setKeepAliveTimeout:600 handler:^{
    //    NSLog(@"Something Happens !");
    //}];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:LOGGED_IN]){
        [self registerTCDeviceWithToken];
        [self changeRootControllerToRevealController];
        //[self performSelector:@selector(getCallDetailsFromService) withObject:nil afterDelay:3.f];

    }
    // Set Up All Notification //
    [self setUpAllNotifications];
    //[self voipRegistration];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [self startReachability];
    
    return YES;
}

-  (void)startReachability {
    Reachability * reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    [reach startNotifier];
}

- (void) reachabilityChanged:(NSNotification *)notification {
    
    Reachability * reach            = [notification object];
    self.internetConnectionReach    = reach;
    switch (reach.currentReachabilityStatus) {
        case NotReachable: {
            [[SharedClass sharedInstance] setIsNetwork:NO];
        }
            break;
        case ReachableViaWiFi:{
            [[SharedClass sharedInstance] setIsNetwork:YES];
            break;
        }
        case ReachableViaWWAN: {
            [[SharedClass sharedInstance] setIsNetwork:YES];
            break;
        }
    
        default: {
            break;
        }
    }
}

// Register for VoIP notifications
/*- (void) voipRegistration {
    // Create a push registry object
    PKPushRegistry * voipRegistry = [[PKPushRegistry alloc] initWithQueue: dispatch_get_main_queue()];
    // Set the registry's delegate to self
    voipRegistry.delegate = self;
    // Set the push type to VoIP
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP]; // Register
}



// Handle updated push credentials
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials: (PKPushCredentials *)credentials forType:(NSString *)type {
    // Register VoIP push token (a property of PKPushCredentials) with server
}

// Handle incoming pushes
- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    // Process the received push
    NSLog(@"Incoming Call");
}
*/



#pragma mark APNS Delegate methods


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Update DeviceToken On Server //
    NSString *deviceTokenStr = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenStr forKey:@"DeviceToken"];
}

// Handle any failure to register.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register for remote notifications: %@", error);
    [[NSUserDefaults standardUserDefaults] setValue:[[UIDevice currentDevice] name] forKey:@"DeviceToken"];
}

// Because alerts don't work when the app is running, the app handles them.
// This uses the userInfo in the payload to display a UIAlertView.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
}


-(void)registerTCDeviceWithToken{
    
    //NSString    * urlString = [NSString stringWithFormat:@"http://52.27.166.47:5000/token"];//https://morning-ridge-70693.herokuapp.com/token
    //NSURL       * url       = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    //NSError     * error     = nil;
    //NSData      * data      = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url]
    //                                                returningResponse:&response error:nil];
    //NSString   * token     = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    //token       = [Utility capabilityToken];
    //phoneDevice             = [[TCDevice alloc] initWithCapabilityToken:token delegate:self];
    [_phone login];
}

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#pragma mark - Change Status Bar -

-(void)changeStatusBarColor{
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        
        UIView * view           = [[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
        view.backgroundColor    = [UIColor whiteColor];
        [self.window.rootViewController.view addSubview:view];
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        // iOS 6.1 or earlier //
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    } else {
        // iOS 7.0 or later //
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTranslucent:NO];
    }
    [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
}

/*
-(NSString *)getUniqueDeviceIdentifierAsString{
    
    NSString * appName              =[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString * strApplicationUUID   = [SSKeychain passwordForService:appName account:@"incoding"];
    if (strApplicationUUID == nil){
        strApplicationUUID          = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"incoding"];
    }
    
    return strApplicationUUID;
}
 */

#pragma mark - Add/Remove Call View To Window -

-(void)addCallViewToWindow:(NSMutableDictionary*)dictUserDetails{
    
    
    _speakerEnable           = NO;
    _muteEnable              = NO;
    
    [_phone setSpeakerEnabled:_speakerEnable];
    [_phone setMuted:_muteEnable];

    [self.window addSubview:[self viewCall]];
    
    NSString * name = nil;
    if([dictUserDetails valueForKeyPath:@"avatar.name"])
        name        = [dictUserDetails valueForKeyPath:@"avatar.name"];
    else
        name        = [dictUserDetails valueForKey:@"name"];
    
    _viewCall.labelUserName.text        = name;
    [_viewCall.buttonCall.layer setBorderColor:[UIColor redColor].CGColor];
    //_viewCall.imageViewUser.image       = [UIImage imageNamed:[dictUserDetails valueForKeyPath:@"avatar.image"]];
    
    _viewCall.labelTimer.timerType = MZTimerLabelTypeStopWatch;
    if([dictUserDetails valueForKey:@"startTimer"] && [[dictUserDetails valueForKey:@"startTimer"] integerValue] == 1){
        [_viewCall.labelTimer start];
        [_viewCall.labelCalling setText:@"Incoming Call ..."];
    }
    else{
        [_viewCall.labelTimer setHidden:YES];
        [_viewCall.labelCalling setText:@"Calling ..."];
        [[_viewCall layer] setValue:[dictUserDetails valueForKey:@"call_charges"] forKey:@"call_charges"];
    }
    
    
    [_viewCall.buttonSpeaker setSelected:_speakerEnable];
    [_viewCall.buttonMute setSelected:_muteEnable];
    
    [_viewCall.buttonCall addTarget:self action:@selector(disconnectCall) forControlEvents:UIControlEventTouchUpInside];
    [_viewCall.buttonSpeaker addTarget:self action:@selector(enableSpeaker) forControlEvents:UIControlEventTouchUpInside];
    [_viewCall.buttonMute addTarget:self action:@selector(enableMute) forControlEvents:UIControlEventTouchUpInside];
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        _viewCall.alpha = 1;
    }
        completion:^(BOOL finished) {
    }];
}

-(void)removeCallViewFromWindow{
    
    if(_viewCall){
        
        /*
        if([[_viewCall layer] valueForKey:@"call_charges"]){
            
            float callCharges           = [[[_viewCall layer] valueForKey:@"call_charges"] floatValue];
            [_viewCall.labelTimer pause];
            NSTimeInterval interval     = [_viewCall.labelTimer getTimeCounted];
            NSTimeInterval timeInterval = interval;
            
            long seconds                = lroundf(timeInterval);
            long hour                   = seconds / 3600;
            long mins                   = (seconds % 3600) / 60;
            long secs                   = seconds % 60;
            
            NSString * hourStr          = [NSString stringWithFormat:@"%ld",hour];
            NSString * minStr           = [NSString stringWithFormat:@"%ld",mins];
            NSString * secStr           = [NSString stringWithFormat:@"%ld",secs];
            
            if(hour < 9)
                hourStr                 = [NSString stringWithFormat:@"0%ld",hour];
            if(mins < 9)
                minStr                  = [NSString stringWithFormat:@"0%ld",mins];
            if(secs < 9)
                secStr                  = [NSString stringWithFormat:@"0%ld",secs];
            
            float totalCost             = (callCharges/3600) * seconds;
            
            if(seconds > 20){
                
                //NSString * message          = [NSString stringWithFormat:@"Call duration: %@:%@:%@\nIP Rate Per Hour: $ %.02f\n Amount Deducted: $ %.02f",hourStr, minStr, secStr,callCharges,totalCost];
                //SHOW_ALERT(APP_NAME, message, nil, @"OK", nil);
                [self showInvoiceWithDetails:nil];
            }
            
            [[_viewCall layer] setValue:nil forKey:@"call_charges"];
        }*/

        [self performSelector:@selector(getCallDetailsFromService) withObject:nil afterDelay:5.f];
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
            _viewCall.alpha = 0;
        }
                         completion:^(BOOL finished) {
                             [_viewCall removeFromSuperview];
                             _viewCall = nil;

                         }];
    }
}

-(void)showInvoiceWithDetails:(NSMutableDictionary*)invoceDetails{
    
    
    //if([[invoceDetails valueForKey:@"status"] isEqualToString:@"completed"]){
    
    InvoiceView * invoice   = [self.window viewWithTag:222];
    if(invoice){
        [invoice removeFromSuperview];
        invoice             = nil;
    }
    
    invoice   = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] objectAtIndex:0];
    invoice.tag             = 222;
    invoice.frame           = self.window.frame;
    invoice.center          = CGPointMake(CGRectGetMidX(self.window.frame), CGRectGetMinY(self.window.frame)-CGRectGetMidY(self.window.frame));
    [self.window addSubview:invoice];
    
    
    float duration              = 0;
    if([Utility validateString:[invoceDetails valueForKey:@"call_duration"]])
        duration                = [[invoceDetails valueForKeyPath:@"call_duration"] floatValue];
        
    /*long seconds                = lroundf(duration);
    long hour                   = seconds / 3600;
    long mins                   = (seconds % 3600) / 60;
    long secs                   = seconds % 60;
    
    NSString * hourStr          = [NSString stringWithFormat:@"%ld",hour];
    NSString * minStr           = [NSString stringWithFormat:@"%ld",mins];
    NSString * secStr           = [NSString stringWithFormat:@"%ld",secs];
    
    if(hour < 9)
        hourStr                 = [NSString stringWithFormat:@"0%ld",hour];
    if(mins < 9)
        minStr                  = [NSString stringWithFormat:@"0%ld",mins];
    if(secs < 9)
        secStr                  = [NSString stringWithFormat:@"0%ld",secs];
    NSString * message          = [NSString stringWithFormat:@"%@:%@:%@",hourStr, minStr, secStr];*/
    
    invoice.labelCallId.text    = [invoceDetails valueForKey:@"call_id"]; // @"_id.$id" - call_sid //
    
    if([Utility validateString:[invoceDetails valueForKey:@"call_by"]])
        invoice.labelFrom.text      = [invoceDetails valueForKey:@"call_by"];
    else
        invoice.labelFrom.text      = [invoceDetails valueForKey:@"-"];
    
    if([Utility validateString:[invoceDetails valueForKeyPath:@"call_to.user"]])
        invoice.labelTo.text        = [invoceDetails valueForKeyPath:@"call_to.user"];
    else
        invoice.labelTo.text        = @"-";
        
    invoice.labelDuration.text      = [Utility createCallDurationString:duration];
    
    
    NSCharacterSet * numericOnly    = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet * myStringSet    = [NSCharacterSet characterSetWithCharactersInString:[invoceDetails valueForKeyPath:@"call_to.user"]];
    if ([numericOnly isSupersetOfSet: myStringSet]){
        //String entirely contains decimal numbers only.
    }
    
    
    invoice.labelHourlyRate.text    = [NSString stringWithFormat:@"$ %@",[invoceDetails valueForKeyPath:@"call_to.hourly_price"]];
    
    float callCharges               = [[invoceDetails valueForKeyPath:@"call_to.hourly_price"] floatValue];
    float totalCost                 = (callCharges/3600) * duration;
    invoice.labelAmount.text        = [NSString stringWithFormat:@"$ %.02f",totalCost];
    
    //if([Utility validateString:[invoceDetails valueForKey:@"price"]])
    //    invoice.labelAmount.text    = [NSString stringWithFormat:@"$ %@",[invoceDetails valueForKey:@"price"]];
    //else
    //    invoice.labelAmount.text    = @"$ 0";
    
    [Utility deleteLastCallInfo];
    [UIView animateWithDuration:0.4f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        invoice.center      = CGPointMake(CGRectGetMidX(self.window.frame),CGRectGetMidY(self.window.frame));
        
    }
                     completion:^(BOOL finished) {
                     }];
    //}
}

#pragma mark - Disconnect Call  -

-(void)disconnectCall{
    [_phone disconnect];
}

#pragma mark - Enable/Disable Speaker -

-(void)enableSpeaker{
    
    if(_speakerEnable){
        _speakerEnable = NO;
        [_phone setSpeakerEnabled:_speakerEnable];
    }
    else{
        _speakerEnable = YES;
        [_phone setSpeakerEnabled:_speakerEnable];
    }
    [_viewCall.buttonSpeaker setSelected:_speakerEnable];
}

#pragma mark - Enable/Disable Mute  -

-(void)enableMute{
    
    if(_muteEnable){
        _muteEnable = NO;
        [_phone setMuted:_muteEnable];
    }
    else{
        _muteEnable = YES;
        [_phone setMuted:_muteEnable];
    }
    [_viewCall.buttonMute setSelected:_muteEnable];
}

#pragma mark - BasicPhone -

-(BasicPhone*)phone{
    
    if(!_phone)
        self.phone = [[BasicPhone alloc] init];
    
    return _phone;
}

#pragma mark - Create Call View -

-(ViewCall*)viewCall{
    
    if(!_viewCall){
        self.viewCall     = [[[NSBundle mainBundle] loadNibNamed:@"ViewCall" owner:self options:nil] objectAtIndex:0];
        _viewCall.tag     = 555;
    }
    _viewCall.alpha       = 0.0f;
    _viewCall.frame       = self.window.frame;
    return _viewCall;
}

#pragma mark - Change Root Controllers -

-(void)changeRootControllerToRevealController{
    
    self.mainNavigationController           = (UINavigationController*)self.window.rootViewController;
    UIStoryboard       * storyboard         = nil;
    storyboard                              = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    RearTableVC        * rearTableVC        = [storyboard instantiateViewControllerWithIdentifier:@"RearTableVC"];
    
    HomeViewController * homeVC             = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    self.mainNavigationController           = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    RevealController   * revealController   = [[RevealController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:rearTableVC];
    self.revealViewController               = revealController;
    self.window.rootViewController          = self.revealViewController;
}

-(void)changeRootControllerLogin{
    
    UIStoryboard            * storyboard    = nil;
    storyboard                              = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    //MainViewController      * homeVC        = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    UINavigationController  * nc            = [storyboard instantiateViewControllerWithIdentifier:@"UINavigationController"];
    self.window.rootViewController          = nc;
}

#pragma mark - Applicatiion State Methods -

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self performSelector:@selector(getCallDetailsFromService) withObject:nil afterDelay:3.f];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    if([self.phone connection])
        [_phone disconnect];
    [self saveContext];
}

#pragma mark - Core Data stack -

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Raman-Kant.GuidanceClub" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GuidanceClub" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GuidanceClub.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - Twilio Methods -

-(BOOL)isForeground{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    return (state == UIApplicationStateActive);
}

-(void)dealloc{
    // Unregister this class from all notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Set Up All Notifications -

-(void)setUpAllNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidStart:) name:BPLoginDidStart object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidFinish:) name:BPLoginDidFinish object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidFailWithError:) name:BPLoginDidFailWithError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionIsConnecting:) name:BPConnectionIsConnecting object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidConnect:) name:BPConnectionDidConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidDisconnect:) name:BPConnectionDidDisconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionIsDisconnecting:) name:BPConnectionIsDisconnecting object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidFailToConnect:) name:BPConnectionDidFailToConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidFailWithError:) name:BPConnectionDidFailWithError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingIncomingConnectionReceived:) name:BPPendingIncomingConnectionReceived object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingIncomingConnectionDidDisconnect:)  name:BPPendingIncomingConnectionDidDisconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidStartListeningForIncomingConnections:) name:BPDeviceDidStartListeningForIncomingConnections object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidStopListeningForIncomingConnections:) name:BPDeviceDidStopListeningForIncomingConnections object:nil];
}

#pragma mark - Notifications Methods -

-(void)addStatusMessage:(NSString*)message{
    NSLog(@"Message = %@",message);
}

-(void)loginDidStart:(NSNotification*)notification{
    [self addStatusMessage:@"- Logging in -"];
}

-(void)loginDidFinish:(NSNotification*)notification{
    
    NSNumber * hasOutgoing = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityOutgoingKey];
    NSNumber * hasIncoming = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityIncomingKey];
   
    if ( [hasOutgoing boolValue] == YES ){
        [self addStatusMessage:@"- Outgoing calls allowed -"];
    }
    else{
        [self addStatusMessage:@"- Unable to make outgoing calls with current capabilities -"];
    }
    if ( [hasIncoming boolValue] == YES ){
        [self addStatusMessage:@"- Incoming calls allowed -"];
    }
    else{
        [self addStatusMessage:@"- Unable to receive incoming calls with current capabilities -"];
    }
    
    [self updateCallViewInterface];
}

-(void)loginDidFailWithError:(NSNotification*)notification{
    
    NSError* error = [[notification userInfo] objectForKey:@"error"];
    
    if ( error ){
        NSString* message = [NSString stringWithFormat:@"- Error logging in: %@ (%ld) -", [error localizedDescription], (long)[error code]];
        [self addStatusMessage:message];
    }
    else{
        [self addStatusMessage:@"- Unknown error logging in -"];
    }
    
    [self updateCallViewInterface];
}

-(void)connectionIsConnecting:(NSNotification*)notification{
    
    [self addStatusMessage:@"- Attempting to connect -"];
    [self updateCallViewInterface];
}

-(void)connectionDidConnect:(NSNotification*)notification
{
    
    NSDictionary * parameters = [notification object];
    [Utility saveLastCallInfo:[parameters mutableCopy]];
    
    [self addStatusMessage:@"- Connection did connect -"];
    [_viewCall.labelTimer setHidden:NO];
    [_viewCall.labelTimer start];
    [self updateCallViewInterface];
    //self.switchMuted.enabled = YES;
    //self.switchMuted.on = NO;
}

-(void)connectionDidFailToConnect:(NSNotification*)notification{
    [self addStatusMessage:@"- Couldn't establish outgoing call -"];
    [self updateCallViewInterface];
}

-(void)connectionIsDisconnecting:(NSNotification*)notification
{
    [self addStatusMessage:@"- Attempting to disconnect -"];
    //[self removeCallViewFromWindow];
    //[self syncMainButton];
}

-(void)connectionDidDisconnect:(NSNotification*)notification{
    
    [self addStatusMessage:@"- Connection did disconnect "];
    //SHOW_ALERT(APP_NAME, @"User currenty no available to take call please try after some time", nil, @"OK", nil);
    dispatch_async (dispatch_get_main_queue(), ^{
        [self removeCallViewFromWindow];
    });
    //[self syncMainButton];
    //self.switchMuted.enabled = NO;
    //self.switchMuted.on = NO;
}

-(void)connectionDidFailWithError:(NSNotification*)notification{
    
    NSError * error = [[notification userInfo] objectForKey:@"error"];
    if ( error ){
        NSString* message = [NSString stringWithFormat:@"- Connection did fail with error code %ld, domain %@ -", (long)[error code], [error domain]];
        [self addStatusMessage:message];
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
            _viewCall.alpha = 0;
        }
                         completion:^(BOOL finished) {
                             
                             
                             dispatch_async (dispatch_get_main_queue(), ^{
                                 [_viewCall removeFromSuperview];
                                 _viewCall = nil;
                             });

                           
                         }];
    }
    //[self syncMainButton];
}

-(void)deviceDidStartListeningForIncomingConnections:(NSNotification*)notification{
    [self addStatusMessage:@"- Device is listening for incoming connections -"];
}

-(void)deviceDidStopListeningForIncomingConnections:(NSNotification*)notification
{
    NSError* error = [[notification userInfo] objectForKey:@"error"]; // May be nil //
    if ( error ){
        [self addStatusMessage:[NSString stringWithFormat:@"- Device is no longer listening for connections due to error: %@ (%ld) -", [error localizedDescription], (long)[error code]]];
    }
    else{
        [self addStatusMessage:@"- Device is no longer listening for connections -"];
    }
}

-(void)pendingIncomingConnectionReceived:(NSNotification*)notification{
    
    NSDictionary* parameters = [notification userInfo];
    // Show alert view asking if user wants to accept or reject call //
    [self performSelectorOnMainThread:@selector(constructAlert:) withObject:parameters waitUntilDone:NO];
    // Check for background support
    if (![self isForeground]){
        // App is not in the foreground, so send LocalNotification //
        UIApplication       * app           = [UIApplication sharedApplication];
        UILocalNotification * notification  = [[UILocalNotification alloc] init];
        NSArray             * oldNots       = [app scheduledLocalNotifications];
        
        if ([oldNots count] > 0){
            [app cancelAllLocalNotifications];
        }
        
        notification.alertBody = @"Incoming Call";
        notification.soundName = @"Sound.mp3";
        [app presentLocalNotificationNow:notification];
    }
    [self addStatusMessage:@"- Received incoming connection -"];
    //[self syncMainButton];
}

-(void)pendingIncomingConnectionDidDisconnect:(NSNotification*)notification{
    
    // Make sure to cancel any pending notifications/alerts //
    [self performSelectorOnMainThread:@selector(cancelAlert) withObject:nil waitUntilDone:NO];
    if ( ![self isForeground] ){
        //App is not in the foreground, so kill the notification we posted. //
        UIApplication   * app = [UIApplication sharedApplication];
        [app cancelAllLocalNotifications];
    }
    
    [self addStatusMessage:@"- Pending connection did disconnect -"];
    [self updateCallViewInterface];
}

#pragma mark - UIAlertView -

-(void)constructAlert:(NSDictionary*)parameters{
    
    NSString * title                = @"Incoming Call from ";
    title                           = [title stringByAppendingString:[parameters objectForKey:TCConnectionIncomingParameterFromKey]];
    alertViewCall                   = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Accept",@"Reject", @"Ignore",nil];
    [alertViewCall show];
}

-(void)cancelAlert{
    
    if(alertViewCall){
        [alertViewCall dismissWithClickedButtonIndex:1 animated:YES];
        alertViewCall = nil;
    }
}



#pragma mark - UIAlertViewDelegate -

- (void)alertView:(UIAlertView* )alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
        // Accept button pressed
        if(!self.phone.connection){
            [self.phone acceptConnection];
            NSString * name = [alertView title];
            
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            [dict setValue:[name stringByReplacingOccurrencesOfString:@"Incoming Call from " withString:@""] forKey:@"name"];
            [dict setValue:@"1" forKey:@"startTimer"];
            [self performSelector:@selector(addCallViewToWindow:) withObject:dict afterDelay:0.2];
        }
        else{
            //A connection already existed, so disconnect old connection and connect to current pending connectioon
            [self.phone disconnect];
            //Give the client time to reset itself, then accept connection
            NSString * name = [alertView title];
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            [dict setValue:[name stringByReplacingOccurrencesOfString:@"Incoming Call from " withString:@""] forKey:@"name"];
            [dict setValue:@"1" forKey:@"startTimer"];
            [self.phone performSelector:@selector(acceptConnection) withObject:nil afterDelay:0.2];
            [self performSelector:@selector(addCallViewToWindow:) withObject:dict afterDelay:0.3];
        }
    }
    else if (buttonIndex == 1){
        // We don't release until after the delegate callback for connectionDidConnect:
        [self.phone rejectIncomingConnection];
    }
    else {
        [self.phone ignoreIncomingConnection];
    }
}


#pragma mark - Update UI -

-(void)updateCallViewInterface{
    
    if ( ![NSThread isMainThread] ){
        [self performSelectorOnMainThread:@selector(updateCallViewInterface) withObject:nil waitUntilDone:NO];
        return;
    }
    // Sync the main button according to the current connection's state //
    if (self.phone.connection) {
        if (self.phone.connection.state == TCConnectionStateDisconnected) {
            //Connection state is closed, show idle button
            //[buttonLogin setImage:[UIImage imageNamed:@"idle"] forState:UIControlStateNormal];
        }
        
        else if (self.phone.connection.state == TCConnectionStateConnected) {
            //Connection state is open, show in progress button
            //[buttonLogin setImage:[UIImage imageNamed:@"inprogress"] forState:UIControlStateNormal];
        }
        
        else if (self.phone.connection.state == TCConnectionStateConnecting) {
            //[buttonLogin setImage:[UIImage imageNamed:@"predialing"] forState:UIControlStateNormal];
        }
        
        else {
            //Connection is in the middle of connecting. Show dialing button
            //[buttonLogin setImage:[UIImage imageNamed:@"dialing"] forState:UIControlStateNormal];
        }
    }
    else {
        if (self.phone.pendingIncomingConnection) {
            //A pending incoming connection existed, show dialing button
            //[buttonLogin setImage:[UIImage imageNamed:@"dialing"] forState:UIControlStateNormal];
        }
        else{
            
            //Both connection and _pending connnection do not exist, show idle button
            //[buttonLogin setImage:[UIImage imageNamed:@"idle"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Web Service To Get Last Call Details -

-(void)getCallDetailsFromService{
    
    //[SVProgressHUD showWithStatus:STATUS_LOADING];
    //NSMutableDictionary * callInfo      = [Utility fetchLastCallInfo];
    NSString            * urlString     = [NSString stringWithFormat:@"%@%@",BASE_URL, CALL_INVOICE];
    //NSString            * post          = [NSString stringWithFormat:@"call_sid=%@",[callInfo valueForKey:@"CallSid"]];
     NSString           * post          = [NSString stringWithFormat:@"av_name=%@&user_no=%@",[Utility clientID],[Utility mobileNumber]];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //[SVProgressHUD dismiss];
        });
        
        /*if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
            });
            return;
        }*/
        
        NSError      * errorJson    = nil;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",responseDict);
        
        
        /*if(!responseDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
            });
            return;
        }*/
        
        if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSMutableDictionary * callDetails = [responseDict valueForKey:@"response"];
                [self showInvoiceWithDetails:callDetails];
            });
            
        }
        /*else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:@"An error encounterd while getting invoice, please check your payments from menu bar." target:self.window.rootViewController];
            });
        }*/
    }];
    [postDataTask resume];

}

@end
