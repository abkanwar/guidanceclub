//
//  PaymentsVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RevealController.h"
#import "ZUUIRevealController.h"

#import "Utility.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@interface PaymentsVC : UIViewController<ZUUIRevealControllerDelegate, NSURLSessionDelegate>{
    
    IBOutlet UITableView        * tableViewPayments;
    NSMutableArray              * arrayPayments;
}

@property (nonatomic, strong) IBOutlet UITableView        * tableViewPayments;

@end
