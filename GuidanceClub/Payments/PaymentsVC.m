//
//  PaymentsVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "PaymentsVC.h"

@interface PaymentsVC (){
    
    float cellHeight;

}

@end

@implementation PaymentsVC
@synthesize tableViewPayments;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.title = @"Payments";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:211/255.0 green:94/255.0 blue:62/255.0 alpha:1.0f]}];
    
    UIBarButtonItem * itemLeftA          = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    NSArray         * arrayLeftBatrItems = [[NSArray alloc]initWithObjects:itemLeftA, nil];
    self.navigationItem.leftBarButtonItems = arrayLeftBatrItems;
 
    //[[APP_DELEGATE  revealViewController] setDelegate:self];
    [APP_DELEGATE revealViewController].enableSwipeAndTapGestures = YES;
    
    self.tableViewPayments.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self fetchPaymentHistoryFromServer:[Utility clientID]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)revealController:(ZUUIRevealController *)revealController willRevealRearViewController:(UIViewController *)rearViewController{
    
    UIView * view               = [[UIView alloc] initWithFrame:self.view.window.frame];
    view.backgroundColor        = [UIColor blackColor];
    view.tag                    = 555;
    view.userInteractionEnabled = YES;
    [self.view addSubview:view];
    view.alpha                  = 0.f;
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        view.alpha                  = 0.35f;
    }
                     completion:^(BOOL finished) {
                         
                     }];
    //self.view.userInteractionEnabled = NO;
}
- (void)revealController:(ZUUIRevealController *)revealController willHideRearViewController:(UIViewController *)rearViewController{
    
    UIView * view = [self.view viewWithTag:555];
    [view removeFromSuperview];
    //self.view.userInteractionEnabled = YES;
}

-(void)fetchPaymentHistoryFromServer:(NSString*)userID{
    
    [SVProgressHUD showWithStatus:STATUS_LOADING];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, USER_PAYMENT];
    NSString * post             = [NSString stringWithFormat:@"av_name=%@", userID];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * tempDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",tempDict);
        
        
        if(!tempDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
            return;
        }
        
        if([[tempDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                arrayPayments = [tempDict valueForKey:@"response"];
                [self.tableViewPayments reloadData];
                [self performSelector:@selector(reloadPaymentsTableView) withObject:nil afterDelay:0.25];
            });
        }
        else if([[tempDict valueForKeyPath:@"status_code"] integerValue] == 401){
            NSString    * meassage = @"Message";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
    }];
    [postDataTask resume];
}

-(void)reloadPaymentsTableView{
    [self.tableViewPayments reloadData];
}

#pragma mark - Table View Data Source & Delegate -


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayPayments count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString * simpleTableIdentifier = @"cellIdentifierC";
    UITableViewCell * cell          = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.backgroundColor            = [UIColor whiteColor];
    cell.selectionStyle             = UITableViewCellSelectionStyleNone;
    
    UILabel * labelCallID           = [cell viewWithTag:2];
    UILabel * labelFrom             = [cell viewWithTag:4];
    UILabel * labelTo               = [cell viewWithTag:6];
    UILabel * labelDuration         = [cell viewWithTag:8];
    UILabel * labelAmount           = [cell viewWithTag:10];
    UILabel * labelStatus           = [cell viewWithTag:12];
    
    
    labelCallID.text                = [[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"call_id"];
    labelFrom.text                  = [[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"call_by"];
    labelTo.text                    = [[arrayPayments objectAtIndex:[indexPath row]] valueForKeyPath:@"call_to.user"];
    
    
    
    float duration              = 0;
    if([Utility validateString:[[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"call_duration"]])
        duration                = [[[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"call_duration"] floatValue];
    labelDuration.text          = [Utility createCallDurationString:duration];
    
    //labelAmount.text            = [Utility validateString:[[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"price"]] ? [NSString stringWithFormat:@"$ %@",[[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"price"]] : @"$0";
    
    float callCharges           = [[[arrayPayments objectAtIndex:[indexPath row]] valueForKeyPath:@"call_to.hourly_price"] floatValue];
    float totalCost             = (callCharges/3600) * duration;
    labelAmount.text            = [NSString stringWithFormat:@"$ %.02f",totalCost];

    labelStatus.text                    = [[arrayPayments objectAtIndex:[indexPath row]] valueForKey:@"status"];
    cellHeight                          = CGRectGetMaxY(labelAmount.frame)+8;
    
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
