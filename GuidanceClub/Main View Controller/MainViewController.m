//
//  MainViewController.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "MainViewController.h"
#import "SVProgressHUD.h"
#import "Utility.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loginButtonAction:(id)sender{

    [self.view endEditing:YES];
    if([self validateAllFields]){
        
        NSString * strEmailAddress = [textFieldEmail text];
        NSString * strPassword     = [textFieldPassword text];
        //NSString * strToken        = [[NSUserDefaults standardUserDefaults] valueForKey:DEVICE_TOKEN];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showWithStatus:STATUS_LOADING];
        });
        
        NSString * urlString = [NSString stringWithFormat:@"%@%@",BASE_URL, LOGIN_URL];
        NSString * post = [NSString stringWithFormat:@"username=%@&password=%@",strEmailAddress, strPassword];
        
        NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [request setHTTPMethod:@"POST"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:requestData];
        
        NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            
            if (error) {
                NSLog(@"Error:%@", [error localizedDescription]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Utility showToastWithMessage:[error localizedDescription] target:self];
                });
                return;
            }
            
            NSError      * errorJson    = nil;
            NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
            NSLog(@"Response Dict = %@ && Error = %@",responseDict, errorJson);
            
            if(!responseDict && [errorJson code] == 3840){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                    [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
                });

            }

            if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 200){
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSMutableDictionary * dictDetails = [[responseDict valueForKeyPath:@"response"] mutableCopy];
                    
                    if([[dictDetails valueForKey:@"profile_completed"] isEqual:[NSNull null]])
                       [dictDetails setValue:@"0" forKey:@"profile_completed"];
                    
                    [Utility saveUserDetails:dictDetails];
                    BOOL isIP = NO;
                    if([[dictDetails valueForKey:@"user_is"] isEqualToString:@"IP"])
                        isIP  = YES;
                    
                    [[NSUserDefaults standardUserDefaults] setBool:isIP forKey:@"is_IP"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [APP_DELEGATE changeRootControllerToRevealController];
                    [self performSelector:@selector(loginUserToTwilio) withObject:nil afterDelay:0.5f];
                    //[[APP_DELEGATE phone] login];
                });
                
            }
            else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 401){
                NSString    * meassage = @"Please specify a valid username or password.";
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Utility showToastWithMessage:meassage target:self];
                });
            }
            else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 203){
                NSString    * meassage = @"You email id in inactive state, please verify your email id.";
                dispatch_async(dispatch_get_main_queue(), ^{
                    [Utility showToastWithMessage:meassage target:self];
                });
            }
        }];
        [postDataTask resume];
    }
}

-(void)loginUserToTwilio{
    [[APP_DELEGATE phone] login];
}

-(BOOL)validateAllFields{
    
    BOOL isValid = NO;
   
    if(![[textFieldEmail text] length]){
        SHOW_ALERT(APP_NAME, @"Please enter email address", nil, @"OK", nil);
        return isValid;
    }
    if(![[textFieldPassword text] length]){
        SHOW_ALERT(APP_NAME, @"Please enter password", nil, @"OK", nil);
        return isValid;
    }
    
    if(![self validateEmailAddress:[textFieldEmail text]]){
        SHOW_ALERT(APP_NAME, @"Please enter valid email address", nil, @"OK", nil);
        return isValid;
    }
    if([[textFieldPassword text] length] < 6){
        SHOW_ALERT(APP_NAME, @"Passwords must contain at least six characters", nil, @"OK", nil);
        return isValid;
    }
    return YES;
}


#pragma mark - Validate Email Address -

-(BOOL)validateEmailAddress:(NSString *)addressString{
    
    BOOL        stricterFilter          = NO;
    NSString    * stricterFilterString  = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString    * laxString             = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString    * emailRegex            = stricterFilter ? stricterFilterString : laxString;
    NSPredicate * emailTest             = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:addressString];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
