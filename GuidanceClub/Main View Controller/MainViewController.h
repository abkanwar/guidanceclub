//
//  MainViewController.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface MainViewController : UIViewController <NSURLSessionDelegate>{
    
    __weak IBOutlet UITextField * textFieldEmail;
    __weak IBOutlet UITextField * textFieldPassword;
    
}

@end
