//
//  SlideHeaderView.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideHeaderView : UIView{
    
}

@property (nonatomic, strong) IBOutlet UILabel      * labelName;
@property (nonatomic, strong) IBOutlet UILabel      * labelLocation;

@property (nonatomic, strong) IBOutlet UIImageView  * imageView;

@property (nonatomic, strong) IBOutlet UIButton     * buttonEdit;

@end
