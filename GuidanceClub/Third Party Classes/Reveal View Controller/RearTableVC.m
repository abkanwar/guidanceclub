//
//  RearTableVC.m
//  Movate
//
//  Created by Raman Kant on 4/23/15.
//  Copyright (c) 2015 Raman Kant. All rights reserved.
//

#import "RearTableVC.h"

#import "Constants.h"
#import "RevealController.h"

#import "HomeViewController.h"
#import "MyAppointmants.h"
#import "PaymentsVC.h"
#import "NotificationsVC.h"
#import "BecomeIP.h"
#import "MyBadgesVC.h"
#import "HelpVC.h"
#import "SettingsVC.h"

#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"

#import "SlideHeaderView.h"

@interface RearTableVC (){
    NSMutableArray                  * arrayTitles;
    NSMutableArray                  * arrayImages;
    
    UIImageView                     * imageViewProfilePic;
}
@property (nonatomic, strong) SlideHeaderView         * slideHeaderView;

@end

@implementation RearTableVC
@synthesize tableViewMenu;

#pragma mark - View Life Cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayTitles = [[NSMutableArray alloc]initWithObjects:@"Ask for Guidance", @"My Appointments", @"Payments",@"Notifications",@"Become an IP",@"My Badges",@"Help",@"Settings",@"Log out", nil];
    //arrayImages = [[NSMutableArray alloc]initWithObjects:@"Dashboard",@"Settings",@"Log Out", nil];
    
}
//#if __has_feature(objc_arc)
//#else
//#endif

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.tableViewMenu setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.tableViewMenu setTableHeaderView:[self slideHeaderView]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeProfilePic:(UIImage * )image{
    imageViewProfilePic.image = image;
}

-(void)changeSelectedIndexWith:(NSInteger)index{
    
    NSIndexPath * indexPath = [tableViewMenu indexPathForSelectedRow];//[NSIndexPath indexPathForRow:2 inSection:0];
    [tableViewMenu deselectRowAtIndexPath:indexPath animated:YES];
    indexPath               = [NSIndexPath indexPathForRow:index inSection:0];
    [tableViewMenu selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
    [self tableView:tableViewMenu didSelectRowAtIndexPath:indexPath];
    
    RevealController * revealController = ([self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil);
    [revealController revealToggle:self];
    
    
    
}

#pragma mark - Table View Data Source & Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayTitles count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * simpleTableIdentifier = @"CellIdentifier";
    UITableViewCell * cell      = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.backgroundColor        = [UIColor clearColor];
    //cell.selectionStyle         = UITableViewCellSelectionStyleNone;
    
    //UIImageView * imageView     = (UIImageView *)[cell.contentView  viewWithTag:1];
    //imageView.image             = [UIImage imageNamed:[arrayImages objectAtIndex:[indexPath row]]];
    
    cell.textLabel.text         = [arrayTitles objectAtIndex:[indexPath row]];
    
    
    switch ([indexPath row]) {
        case 0:
            break;
        case 1:
            cell.textLabel.textColor = [UIColor darkGrayColor];
            break;
        case 2:
            break;
        case 3:
            cell.textLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            break;
        case 5:
            cell.textLabel.textColor = [UIColor darkGrayColor];
            break;
        case 6:
            cell.textLabel.textColor = [UIColor darkGrayColor];
            break;
        case 7:
            cell.textLabel.textColor = [UIColor darkGrayColor];
            break;
        case 8:
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //[SVProgressHUD showWithStatus:STATUS_LOADING];
    
    RevealController * revealController = ([self.parentViewController isKindOfClass:[RevealController class]]
                                           ? (RevealController *)self.parentViewController : nil);

    switch ([indexPath row]) {
        case 0:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[HomeViewController class]]){
                    HomeViewController * homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }
        }
            break;
            
        case 1:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[MyAppointmants class]]){
                    MyAppointmants * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAppointmants"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 2:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[PaymentsVC class]]){
                    PaymentsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentsVC"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
            
            
        case 3:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[NotificationsVC class]]){
                    NotificationsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationsVC"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 4:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[BecomeIP class]]){
                    BecomeIP * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"BecomeIP"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 5:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[MyBadgesVC class]]){
                    MyBadgesVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyBadgesVC"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 6:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[HelpVC class]]){
                    HelpVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 7:{
            if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] &&
                ![((UINavigationController*)revealController.frontViewController).topViewController
                  isKindOfClass:[HomeViewController class]]){
                    SettingsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
                    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
                    [revealController setFrontViewController:navController animated:NO];
                }
            else{
                [revealController revealToggle:self];
            }

        }
            break;
        case 8:{
            UIActionSheet * popup = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Log out" otherButtonTitles:@"Cancel",nil];
            popup.tag = 100;
            [popup showInView:[UIApplication sharedApplication].keyWindow];
        }
            break;
        default:
            break;
    }
     //[SVProgressHUD dismiss];
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    NSInteger viewWidth        = CGRectGetWidth(self.view.frame);
    NSInteger headerViewHeight  = viewWidth * 0.55;
    return headerViewHeight;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [self tableHeaderView];
}*/

-(SlideHeaderView*)slideHeaderView{
    
    if(!_slideHeaderView){
        
        self.slideHeaderView                = [[[NSBundle mainBundle] loadNibNamed:@"SlideHeaderView" owner:self options:nil] objectAtIndex:0];
        _slideHeaderView.backgroundColor    = [UIColor whiteColor];
        //NSInteger     viewWidth             = CGRectGetWidth(self.view.frame);
        //NSInteger     headerViewHeight      = viewWidth * 0.6;
        _slideHeaderView.frame              = CGRectMake(0, 0, 250, 200);
    }
    
    _slideHeaderView.labelName.text         = [Utility clientID];
    
    return _slideHeaderView;
}

- (void)handleTapFrom: (UITapGestureRecognizer *)recognizer{
    
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:APP_NAME delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    [actionSheet addButtonWithTitle:@"Library"];
    [actionSheet addButtonWithTitle:@"Camera"];
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:LOGGED_IN];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [APP_DELEGATE changeRootControllerLogin];
    }
}


#pragma mark - Buttons Action -

/*-(IBAction)topButtonAction:(id)sender{
    
    UIActionSheet * popup = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Log out" otherButtonTitles:@"Cancel",nil];
    popup.tag = 100;
    [popup showInView:[UIApplication sharedApplication].keyWindow];

}*/

-(void)topButtonAction{
    
    UIActionSheet * popup = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Log out" otherButtonTitles:@"Cancel",nil];
    popup.tag = 100;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    if ([navigationController.viewControllers count] == 3){
        CGFloat   screenHeight  = [[UIScreen mainScreen] bounds].size.height;
        UIView  * plCropOverlay = [[[viewController.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
        plCropOverlay.hidden    = YES;
        int position            = 0;
        
        if (screenHeight == 568)
            position = 124+20;
        else
            position = 80+20;
        CAShapeLayer * circleLayer  = [CAShapeLayer layer];
        UIBezierPath * path2        = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(20.f, position, 280.0f, 280.0f)];
        [path2 setUsesEvenOddFillRule:YES];
        [circleLayer setPath:[path2 CGPath]];
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        UIBezierPath * path         = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 320, screenHeight-72) cornerRadius:0];
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer * fillLayer    = [CAShapeLayer layer];
        fillLayer.path              = path.CGPath;
        fillLayer.fillRule          = kCAFillRuleEvenOdd;
        fillLayer.fillColor         = [UIColor blackColor].CGColor;
        fillLayer.opacity           = 0.5f;
        [viewController.view.layer addSublayer:fillLayer];
        
        UILabel     * moveLabel     = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 320, 50)];
        [moveLabel setText:@"Move and Scale"];
        [moveLabel setTextAlignment:NSTextAlignmentCenter];
        [moveLabel setTextColor:[UIColor whiteColor]];
        [viewController.view addSubview:moveLabel];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
