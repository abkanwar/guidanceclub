//
//  RearTableVC.h
//  Movate
//
//  Created by Raman Kant on 4/23/15.
//  Copyright (c) 2015 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearTableVC : UIViewController <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    IBOutlet UITableView * tableViewMenu;
}
@property(nonatomic, strong)IBOutlet UITableView * tableViewMenu;

-(void)changeSelectedIndexWith:(NSInteger)index;
-(void)changeProfilePic:(UIImage * )image;

@end
