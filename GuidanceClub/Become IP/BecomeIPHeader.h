//
//  BecomeIPHeader.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/18/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BecomeIPHeader : UIView{
    
    IBOutlet UIButton                               * buttonAdd;
    IBOutlet UILabel                                * labelTextHeder;
}
@property (nonatomic, strong) IBOutlet UIButton     * buttonAdd;
@property (nonatomic, strong) IBOutlet UILabel      * labelTextHeder;


@end
