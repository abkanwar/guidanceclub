//
//  SchoolDetailsVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "SchoolDetailsVC.h"

@interface SchoolDetailsVC (){
    
}

@property(nonatomic, strong) UIDatePicker       * datePicker;
@property(nonatomic, strong) UIPickerView       * countryPicker;
@property(nonatomic, strong) NSMutableArray     * arrayCountries;
@property(nonatomic, strong) NSMutableArray     * arrayStates;
@property(nonatomic, strong) NSString           * countryID;
@property(nonatomic, strong) NSString           * stateID;


@end

@implementation SchoolDetailsVC
@synthesize arrayUserInfo, updateSection, updateRow, editMode;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Schooling Details";
    
    //NSLocale        * locale            = [NSLocale currentLocale];
    //NSArray         * countryArray      = [NSLocale ISOCountryCodes];
    //_arrayCountries                     = [[NSMutableArray alloc] init];
    
    //for (NSString * countryCode in countryArray) {
    //    NSString * displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
    //    [_arrayCountries addObject:displayNameString];
        
    //}
    //[_arrayCountries sortUsingSelector:@selector(localizedCompare:)];
    
    //NSString    * countryCode               = [locale objectForKey:NSLocaleCountryCode];
    //NSString    * country                   = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
    //NSUInteger   index                      = [_arrayCountries indexOfObject:country];
    
    //textFieldCountry.inputView = self.countryPicker;
    //textFieldCountry.text      = [_arrayCountries objectAtIndex:index];
    //[_countryPicker selectRow:index inComponent:0 animated:YES];

    
    _arrayCountries             = [[NSMutableArray alloc] init];
    _arrayCountries             = [Utility fetchCountryList];
    
    _arrayStates                = [[NSMutableArray alloc] init];
    
    textFieldCountry.inputView  = self.countryPicker;
    textFieldState.inputView    = self.countryPicker;
    
    _countryID = @"-1";
    _stateID   = @"-1";


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame), CGRectGetMaxY(textFieldCity.frame)+8);
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    textFieldYearFrom.inputView = self.datePicker;
    textFieldYearTo.inputView   = self.datePicker;
    
    if(editMode){
        
        NSMutableDictionary * dataDict = [[arrayUserInfo objectAtIndex:updateSection] objectAtIndex:updateRow];
        textFieldSchoolName.text        = [dataDict valueForKey:@"title"];
        textFieldYearFrom.text          = [dataDict valueForKey:@"yearFrom"];
        textFieldYearTo.text            = [dataDict valueForKey:@"yearTo"];
        textFieldZipCode.text           = [dataDict valueForKey:@"zipCode"];
        textFieldCountry.text           = [dataDict valueForKey:@"country"];
        textFieldState.text             = [dataDict valueForKey:@"state"];
        textFieldCity.text              = [dataDict valueForKey:@"city"];
        textFieldGrade.text             = [dataDict valueForKey:@"grade"];
    }
}


- (void)setTitle:(NSString *)title {
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setLineBreakMode:NSLineBreakByClipping];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:COMMON_COLOR];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setText:title];
    [titleLabel sizeToFit];
    [self navigationItem].titleView = titleLabel;
}

- (UIDatePicker *)datePicker {
    
    if (_datePicker == nil) {
        
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame), 216)];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        _datePicker.maximumDate = [NSDate date];
    }
    return _datePicker;
}


-(void)onDatePickerValueChanged:(UIDatePicker*)datePicker{
    
    if([textFieldYearFrom isFirstResponder])
        textFieldYearFrom.text = [NSDate stringFromDate:datePicker.date withFormat:@"yyyy-MM-dd"];
    else
        textFieldYearTo.text = [NSDate stringFromDate:datePicker.date withFormat:@"yyyy-MM-dd"];
}

-(BOOL)validateAllFields{
    
    BOOL valid = YES;
    if(![textFieldSchoolName.text length] || ![textFieldYearFrom.text length] || ![textFieldYearTo.text length] || ![textFieldZipCode.text length] || ![textFieldCountry.text length] || ![textFieldCity.text length]){
        SHOW_ALERT(APP_NAME, @"Please fill All fields", nil, @"OK", nil);
        valid = NO;
    }
    return valid;
}


-(IBAction)saveMethod:(id)sender{
    
    if(![self validateAllFields])
        return;
    
    
    NSMutableDictionary * dictData = [[NSMutableDictionary alloc] init];
    [dictData setValue:textFieldSchoolName.text forKey:@"title"];
    [dictData setValue:textFieldYearFrom.text forKey:@"yearFrom"];
    [dictData setValue:textFieldYearTo.text forKey:@"yearTo"];
    [dictData setValue:textFieldZipCode.text forKey:@"zipCode"];
    [dictData setValue:textFieldCountry.text forKey:@"country"];
    [dictData setValue:textFieldState.text forKey:@"state"];
    [dictData setValue:textFieldCity.text forKey:@"city"];
    [dictData setValue:textFieldGrade.text forKey:@"grade"];
    [dictData setValue:@"school" forKey:@"detailsType"];
    
    
    //if(editMode)
    //    [[arrayUserInfo objectAtIndex:updateSection]replaceObjectAtIndex:updateRow dictData];
    //else
    //    [[arrayUserInfo objectAtIndex:updateSection] dictData];
    //[self.navigationController popToRootViewControllerAnimated:YES];
    
    [self saveUserSchoolDetailsOnServer:dictData];
}

-(void)saveUserSchoolDetailsOnServer:(NSMutableDictionary*)dictData{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:STATUS_LOADING];
    });
    
    NSString * skillsSet        = [[dictData valueForKey:@"description"] stringByReplacingOccurrencesOfString:@" " withString:@","];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, BECOME_IP];
    NSString * post             = [NSString stringWithFormat:@"profile_id=schools&user_id=%@&school=%@&class=%@&from=%@&to=%@&call_charges=0&skills=%@&country=%@&state=%@&city=%@&zipcode=%@", [Utility userID],[dictData valueForKey:@"title"],[dictData valueForKey:@"grade"],[dictData valueForKey:@"yearFrom"],[dictData valueForKey:@"yearTo"],skillsSet,_countryID,_stateID,[dictData valueForKey:@"city"], [dictData valueForKey:@"zipCode"]];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",responseDict);
        
        
        if(!responseDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
           return; 
        }
        
        if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString    * meassage = @"Schooling details saved successfully.";
                [Utility showToastWithMessage:meassage target:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationUserDetails" object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        }
        else {
            NSString    * meassage = @"An error encountered while saving data please try again!";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
    }];
    [postDataTask resume];
    
}


- (UIPickerView *)countryPicker {
    
    if (_countryPicker == nil) {
        
        self.countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 216, CGRectGetWidth(self.view.frame), 216)];
        _countryPicker.delegate = self;
    }
    return _countryPicker;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if([textFieldCountry isFirstResponder])
        return [_arrayCountries count];
    else
        return [_arrayStates count];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([textFieldCountry isFirstResponder])
        return [[_arrayCountries objectAtIndex:row] valueForKey:@"name"];
    else
        return [[_arrayStates objectAtIndex:row] valueForKey:@"name"];

}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if([textFieldCountry isFirstResponder]){
        
        textFieldCountry.text = [[_arrayCountries objectAtIndex:row] valueForKey:@"name"];
        textFieldState.text   = @"";
        _countryID            = [[_arrayCountries objectAtIndex:row] valueForKey:@"countryId"];
        _arrayStates          = [Utility fetchStateListWithCoutryID:[[_arrayCountries objectAtIndex:row] valueForKey:@"countryId"]];
        
    }else{
        if([_arrayStates count]){
            textFieldState.text = [[_arrayStates objectAtIndex:row] valueForKey:@"name"];
            _stateID            = [[_arrayStates objectAtIndex:row] valueForKey:@"stateId"];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    /*if(textField == textFieldCountry && [textFieldCountry isFirstResponder]){
     
     NSLocale    * locale                = [NSLocale currentLocale];
     NSString    * countryCode           = [locale objectForKey:NSLocaleCountryCode];
     NSString    * country               = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
     NSArray     * arrayFilteredState    = [_arrayCountries filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", country]];
     NSUInteger   index          = [_arrayCountries indexOfObject:[arrayFilteredState objectAtIndex:0]];
     [_countryPicker selectRow:index inComponent:0 animated:YES];
     }*/
    
    /*
     if(textField == textFieldState && [textFieldState isFirstResponder] && ![[textFieldCountry text] length]){
     
     //[Utility showToastWithMessage:@"Please select country" target:self];
     SHOW_ALERT(APP_NAME, @"Please select country first to choose state", self, @"OK", nil);
     [self reloadPickerView];
     return NO;
     }
     
     if(textField == textFieldState && [textFieldState isFirstResponder] && [[textFieldCountry text] length]){
     
     if(![_arrayStates count]){
     //[Utility showToastWithMessage:@"Please select country" target:self];
     SHOW_ALERT(APP_NAME, @"No State to select", self, @"OK", nil);
     [self reloadPickerView];
     return NO;
     }else{
     [self reloadPickerView];
     }
     }
     if(textField == textFieldCountry && [textFieldCountry isFirstResponder]){
     [self reloadPickerView];
     }*/
    [self reloadPickerView];
    return YES;
}


-(void)reloadPickerView{
    [_countryPicker reloadAllComponents];
    [_countryPicker selectRow:0 inComponent:0 animated:YES];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    [self.view endEditing:YES];
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
