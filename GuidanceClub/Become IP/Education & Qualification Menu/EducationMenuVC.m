//
//  EducationMenuVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "EducationMenuVC.h"
#import "SchoolDetailsVC.h"
#import "CollegeDetailsVC.h"

@interface EducationMenuVC ()

@end

@implementation EducationMenuVC
@synthesize arrayUserInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Educational Qualifications";

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}

- (void)setTitle:(NSString *)title {
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setLineBreakMode:NSLineBreakByClipping];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:COMMON_COLOR];
    [titleLabel setText:title];
    [titleLabel sizeToFit];
    [self navigationItem].titleView = titleLabel;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)commonMethod:(id)sender{
    
    switch ([sender tag]) {
        case 100:{
            SchoolDetailsVC * vc  = [self.storyboard instantiateViewControllerWithIdentifier:@"SchoolDetailsVC"];
            vc.arrayUserInfo      = arrayUserInfo;
            vc.editMode           = NO;
            vc.updateSection      = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }
            
            break;
        case 101:{
            CollegeDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CollegeDetailsVC"];
            vc.arrayUserInfo      = arrayUserInfo;
            vc.editMode           = NO;
            vc.updateSection      = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
    
}

-(IBAction)saveMethod:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
