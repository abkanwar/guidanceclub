//
//  ProfessionalDetailsVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NSDate+Helper.h>

#import "Constants.h"
#import "SAMTextView.h"

#import "Utility.h"
#import "Constants.h"
#import "SVProgressHUD.h"

typedef void(^ProfessionalDetailsVCBlock)();

@interface ProfessionalDetailsVC : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, NSURLSessionDelegate>{
    
    __weak IBOutlet UIScrollView    * scrollView;
    __weak IBOutlet UIButton        * buttonSave;
    
    __weak IBOutlet UITextField     * textFieldCourseTitle;
    __weak IBOutlet UITextField     * textFieldYearFrom;
    __weak IBOutlet UITextField     * textFieldYearTo;
    
    __weak IBOutlet UITextField     * textFieldOrgnisationName;
    __weak IBOutlet UITextField     * textFieldZipCode;
    __weak IBOutlet UITextField     * textFieldCountry;
    __weak IBOutlet UITextField     * textFieldState;
    __weak IBOutlet UITextField     * textFieldCity;
    __weak IBOutlet UITextField     * textFieldCertification;
    
    __weak IBOutlet UIImageView     * imageViewBackTView;
    __weak IBOutlet SAMTextView     * textView;

}

@property (nonatomic, strong) NSMutableArray * arrayUserInfo;
@property (readwrite) BOOL                     editMode;
@property (readwrite) NSUInteger               updateSection;
@property (readwrite) NSUInteger               updateRow;

@property (nonatomic, strong) ProfessionalDetailsVCBlock professionalDetailsVCBlock;

@end
