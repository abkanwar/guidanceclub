//
//  InvoiceView.m
//  GuidanceClub
//
//  Created by Raman Kant on 2/15/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "InvoiceView.h"

@implementation InvoiceView
@synthesize labelCallId,
            labelFrom,
            labelTo,
            labelHourlyRate,
            labelDuration,
            labelAmount;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(IBAction)buttonOK:(id)sender{
    
    [UIView animateWithDuration:0.4f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        self.center      = CGPointMake(CGRectGetMidX(self.window.frame),CGRectGetMaxY(self.window.frame)+CGRectGetMidY(self.window.frame));
        
    }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];

                     }];
    
}

@end
