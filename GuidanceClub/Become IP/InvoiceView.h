//
//  InvoiceView.h
//  GuidanceClub
//
//  Created by Raman Kant on 2/15/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoiceView : UIView{
    
}

@property(nonatomic, strong) IBOutlet UILabel   * labelCallId;
@property(nonatomic, strong) IBOutlet UILabel   * labelFrom;
@property(nonatomic, strong) IBOutlet UILabel   * labelTo;
@property(nonatomic, strong) IBOutlet UILabel   * labelHourlyRate;
@property(nonatomic, strong) IBOutlet UILabel   * labelDuration;
@property(nonatomic, strong) IBOutlet UILabel   * labelAmount;

@property(nonatomic, strong) IBOutlet UIButton  * buutonOK;

@end
