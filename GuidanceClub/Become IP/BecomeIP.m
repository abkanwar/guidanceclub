//
//  BecomeIP.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "BecomeIP.h"
#import "JobDetailsVC.h"
#import "EducationMenuVC.h"
#import "HobbiesVC.h"
#import "ProfessionalDetailsVC.h"
#import "SchoolDetailsVC.h"
#import "CollegeDetailsVC.h"


#define PADDING 8.f

@interface BecomeIP (){
    
    NSMutableDictionary * dictIPDetails;
    
}

@end

@implementation BecomeIP

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayUserInfo = [[NSMutableArray alloc] init];
    for (int i = 0; i < 4; i++) {
        NSMutableArray * tempArray = [[NSMutableArray alloc] init];
        [arrayUserInfo addObject:tempArray];
    }
    tableViewIPProfile.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if([Utility is_IP]){
        [self fetchUserDetailsFromServer];
    }else{
        
        
        NSMutableArray * arrayA  = [[NSMutableArray alloc] init];
        
        UIImageView * imageViewA = [self.view viewWithTag:555];
        CGRect rect              = imageViewA.frame;
        rect.origin.x            = rect.origin.x+3;
        rect.origin.y            = rect.origin.y+3;
        rect.size.width          = rect.size.width-6;
        rect.size.height         = rect.size.height-6;
        
        
        textViewPaidSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayA baseColor:[UIColor darkGrayColor]];
        [self.view addSubview:textViewPaidSkillTags];
        
        
        NSMutableArray * arrayB  = [[NSMutableArray alloc] init];
        UIImageView * imageViewB = [self.view viewWithTag:666];
        rect                     = imageViewB.frame;
        rect.origin.x            = rect.origin.x+3;
        rect.origin.y            = rect.origin.y+3;
        rect.size.width          = rect.size.width-6;
        rect.size.height         = rect.size.height-6;
        
        textViewFreeSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayB baseColor:[UIColor darkGrayColor]];
        [self.view addSubview:textViewFreeSkillTags];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchUserDetailsFromServer) name:@"notificationUserDetails" object:nil];
    
    /*textViewFreeSkillTags.colorTag                = [UIColor darkGrayColor];
    textViewFreeSkillTags.colorTagBg              = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0f];
    textViewFreeSkillTags.colorInput              = COLORRGB(0x2ab44e);
    textViewFreeSkillTags.colorInputBg            = COLORRGB(0xffffff);
    textViewFreeSkillTags.colorInputPlaceholder   = COLORRGB(0x2ab44e);
    textViewFreeSkillTags.backgroundColor         = [UIColor clearColor];
    textViewFreeSkillTags.colorInputBoard         = COLORRGB(0x2ab44e);
    textViewFreeSkillTags.viewMaxHeight           = 71;//CGRectGetHeight(textViewFreeSkillTags.frame);
    //textViewFreeSkillTags.tagHeight               = 25;
    textViewFreeSkillTags.type                    = 1;
    textViewFreeSkillTags.tag                     = 555;
    textViewFreeSkillTags.delegate                = self;
    //[textViewFreeSkillTags addTags:@[@"engle",@"Aakash", @"snake",@"Raman",]];
    
    textViewPaidSkillTags.colorTag                = [UIColor darkGrayColor];
    textViewPaidSkillTags.colorTagBg              = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0f];
    textViewPaidSkillTags.colorInput              = COLORRGB(0x2ab44e);
    textViewPaidSkillTags.colorInputBg            = COLORRGB(0xffffff);
    textViewPaidSkillTags.colorInputPlaceholder   = COLORRGB(0x2ab44e);
    textViewPaidSkillTags.backgroundColor         = [UIColor clearColor];
    textViewPaidSkillTags.colorInputBoard         = COLORRGB(0x2ab44e);
    textViewPaidSkillTags.viewMaxHeight           = 71;//CGRectGetHeight(textViewPaidSkillTags.frame);
    //textViewPaidSkillTags.tagHeight               = 25;
    textViewPaidSkillTags.type                    = 1;
    textViewPaidSkillTags.tag                     = 555;
    textViewPaidSkillTags.delegate                = self;
    //[textViewPaidSkillTags addTags:@[@"engle",@"Aakash", @"snake",@"Raman",]];
    
    //NSMutableArray * aarayTags = [[self.comicDetails valueForKey:@"tags"] mutableCopy];
    //[aarayTags removeObject:@""];
    //[aarayTags removeObject:@" "];
    //[self.tagView addTags:aarayTags];*/
}

//-(void)viewDidLayoutSubviews{
    
//    [super viewDidLayoutSubviews];
    
    //CGRect frame                = textViewFreeSkillTags.frame;
    //frame.size.height           = 71;
    //textViewFreeSkillTags.frame = frame;
    
    
    //frame                       = textViewPaidSkillTags.frame;
    //frame.size.height           = 71;
    //textViewPaidSkillTags.frame = frame;
//}

//-(BOOL)willRemoveTag:(EYTagView*)tagView index:(NSInteger)index{
//    return YES;
//}

//-(void)heightDidChangedTagView:(EYTagView *)tagView{
//    NSLog(@"heightDidChangedTagView");
//}


-(void)fetchUserDetailsFromServer{
    
    [SVProgressHUD showWithStatus:STATUS_LOADING];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, USER_PROFILE];
    NSString * post             = [NSString stringWithFormat:@"id=%@", [Utility userID]];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            
            NSMutableArray * arrayA  = [[NSMutableArray alloc] init];
            
            UIImageView * imageViewA = [self.view viewWithTag:555];
            CGRect rect              = imageViewA.frame;
            rect.origin.x            = rect.origin.x+3;
            rect.origin.y            = rect.origin.y+3;
            rect.size.width          = rect.size.width-6;
            rect.size.height         = rect.size.height-6;
            
            
            textViewPaidSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayA baseColor:[UIColor darkGrayColor]];
            [self.view addSubview:textViewPaidSkillTags];
            
            
            NSMutableArray * arrayB  = [[NSMutableArray alloc] init];
            UIImageView * imageViewB = [self.view viewWithTag:666];
            rect                     = imageViewB.frame;
            rect.origin.x            = rect.origin.x+3;
            rect.origin.y            = rect.origin.y+3;
            rect.size.width          = rect.size.width-6;
            rect.size.height         = rect.size.height-6;
            
            textViewFreeSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayB baseColor:[UIColor darkGrayColor]];
            [self.view addSubview:textViewFreeSkillTags];
            
            return;
        }
        
        NSError      * errorJson         = nil;
        NSMutableDictionary * tempDict   = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@ && Error = %@",tempDict, errorJson);
        
        if(!tempDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
            
        }
        
        if([[tempDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                dictIPDetails               = [tempDict valueForKey:@"response"];
                textFieldRate.text          = [NSString stringWithFormat:@"%@",[dictIPDetails valueForKey:@"call_charges"]];
                
                if(![[NSString stringWithFormat:@"%@",[dictIPDetails valueForKey:@"paid_consutancy"]] isEqual:[NSNull null]]){
                    
                    
                    NSMutableArray * arrayTemp = [[[dictIPDetails valueForKey:@"paid_consutancy"] componentsSeparatedByString:@","] mutableCopy];
                    
                    UIImageView * imageViewA = [self.view viewWithTag:555];
                    CGRect rect              = imageViewA.frame;
                    rect.origin.x            = rect.origin.x+2;
                    rect.origin.y            = rect.origin.y+2;
                    rect.size.width          = rect.size.width-4;
                    rect.size.height         = rect.size.height-4;
                    
                    
                    textViewPaidSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayTemp baseColor:[UIColor darkGrayColor]];
                    [self.view addSubview:textViewPaidSkillTags];
                    
                    
                }
                if(![[NSString stringWithFormat:@"%@",[dictIPDetails valueForKey:@"interests"]] isEqual:@"<null>"]){
                    
                    NSMutableArray * arrayTemp = [[[dictIPDetails valueForKey:@"interests"] componentsSeparatedByString:@","] mutableCopy];
                   
                    UIImageView * imageViewB = [self.view viewWithTag:666];
                    CGRect rect              = imageViewB.frame;
                    rect.origin.x            = rect.origin.x+2;
                    rect.origin.y            = rect.origin.y+2;
                    rect.size.width          = rect.size.width-4;
                    rect.size.height         = rect.size.height-4;
                    
                    textViewFreeSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayTemp baseColor:[UIColor darkGrayColor]];
                    [self.view addSubview:textViewFreeSkillTags];

                }else{
                    NSMutableArray * arrayTemp = [[NSMutableArray alloc] init];
                    
                    UIImageView * imageViewB = [self.view viewWithTag:666];
                    CGRect rect              = imageViewB.frame;
                    rect.origin.x            = rect.origin.x+2;
                    rect.origin.y            = rect.origin.y+2;
                    rect.size.width          = rect.size.width-4;
                    rect.size.height         = rect.size.height-4;
                    
                    textViewFreeSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayTemp baseColor:[UIColor darkGrayColor]];
                    [self.view addSubview:textViewFreeSkillTags];
                }
                
                //if(![[NSString stringWithFormat:@"%@",[dictIPDetails valueForKey:@"interests"]] isEqual:[NSNull null]])
                //    textViewPaidSkillTags.text      = [NSString stringWithFormat:@"%@",[dictIPDetails valueForKey:@"interests"]];
                
                
                NSMutableArray * arrayJobs      = [tempDict valueForKeyPath:@"response.jobs"];
                if(arrayJobs !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:0 withObject:[arrayJobs mutableCopy]];
                
                NSMutableArray * arrayCollege   = [tempDict valueForKeyPath:@"response.colleges"];
                if(arrayCollege !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:1 withObject:[arrayCollege mutableCopy]];
                
                
                NSMutableArray * arraySchool      = [tempDict valueForKeyPath:@"response.schools"];
                if(arraySchool !=  (id)[NSNull null]){
                    [[arrayUserInfo objectAtIndex:1] addObjectsFromArray:[arraySchool mutableCopy]];
                }
                
                NSMutableArray * arraycertifications  = [tempDict valueForKeyPath:@"response.certifications"];
                if(arraycertifications !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:2 withObject:[arraycertifications mutableCopy]];
                
                NSLog(@"Jobs = %@", arrayJobs);
                [tableViewIPProfile reloadData];
                
            });
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:@"An error occur while getting your profile info please try again!" target:self];
                
                NSMutableArray * arrayA  = [[NSMutableArray alloc] init];
                
                UIImageView * imageViewA = [self.view viewWithTag:555];
                CGRect rect              = imageViewA.frame;
                rect.origin.x            = rect.origin.x+3;
                rect.origin.y            = rect.origin.y+3;
                rect.size.width          = rect.size.width-6;
                rect.size.height         = rect.size.height-6;
                
                
                textViewPaidSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayA baseColor:[UIColor darkGrayColor]];
                [self.view addSubview:textViewPaidSkillTags];
                
                
                NSMutableArray * arrayB  = [[NSMutableArray alloc] init];
                UIImageView * imageViewB = [self.view viewWithTag:666];
                rect                     = imageViewB.frame;
                rect.origin.x            = rect.origin.x+3;
                rect.origin.y            = rect.origin.y+3;
                rect.size.width          = rect.size.width-6;
                rect.size.height         = rect.size.height-6;
                
                textViewFreeSkillTags = [[JLTTagListView alloc]initWithFrame:rect tags:arrayB baseColor:[UIColor darkGrayColor]];
                [self.view addSubview:textViewFreeSkillTags];
            });
        }
    }];
    [postDataTask resume];
}

-(IBAction)saveAction:(id)sender{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:STATUS_LOADING];
    });
    
    
    NSMutableArray * arrayA = textViewPaidSkillTags.tags;
    
    
    NSString * stringPaidTags       = [arrayA componentsJoinedByString: @","];//[textViewPaidSkillTags text]
    NSString * stringPaidTagsFinal  = [stringPaidTags stringByReplacingOccurrencesOfString:@" " withString:@","];
    
    NSMutableArray * arrayB         = textViewFreeSkillTags.tags;
    NSString * stringFreeTags       = [arrayB componentsJoinedByString: @","];//[textViewFreeSkillTags text]
    NSString * stringFreeTagsFinal  = [stringFreeTags stringByReplacingOccurrencesOfString:@" " withString:@","];
    
    NSString * urlString            = [NSString stringWithFormat:@"%@%@",BASE_URL, BECOME_IP];
    NSString * post                 = [NSString stringWithFormat:@"user_type=IP&call_charges=%@&paid_consultancy=%@&user_id=%@&profile_id=is_ip&skills=%@", [textFieldRate text], stringPaidTagsFinal, [Utility userID], stringFreeTagsFinal];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@ && Error = %@",responseDict, errorJson);
        
        if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString    * meassage = @"User profile updated successfully.";
                 [Utility showToastWithMessage:meassage target:self];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_IP"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            });
            
        }
        else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 401){
            NSString    * meassage = @"Please specify a valid username or password.";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
        else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 401){
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:@"An error encountered please try again." target:self];
            });
        }
    }];
    [postDataTask resume];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    //textViewPaidSkillTags.placeholder       = @"Enter Paid Consultancy Skills(e.g Software Developer IT)";
    textViewPaidSkillTags.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //textViewFreeSkillTags.placeholder       = @"Enter Free Consultancy Skills(e.g Software Developer IT)";
    textViewFreeSkillTags.layer.borderColor = [UIColor lightGrayColor].CGColor;

    
    self.title = @"Become an IP";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:211/255.0 green:94/255.0 blue:62/255.0 alpha:1.0f]}];
    
    UIBarButtonItem * itemLeftA          = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    NSArray         * arrayLeftBatrItems = [[NSArray alloc]initWithObjects:itemLeftA, nil];
    self.navigationItem.leftBarButtonItems = arrayLeftBatrItems;
    
    [tableViewIPProfile reloadData];
    
    //[[APP_DELEGATE  revealViewController] setDelegate:self];
    [APP_DELEGATE revealViewController].enableSwipeAndTapGestures = YES;
}

- (void)setTitle:(NSString *)title {
    
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setLineBreakMode:NSLineBreakByClipping];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setTextColor:COMMON_COLOR];
    [titleLabel setText:title];
    [titleLabel sizeToFit];
    [self navigationItem].titleView = titleLabel;
}

-(IBAction)commonMethod:(id)sender{
    
    switch ([sender tag]) {
        
        case 100:{
            JobDetailsVC * vc   = [self.storyboard instantiateViewControllerWithIdentifier:@"JobDetailsVC"];
            vc.editMode         = NO;
            vc.updateSection    = 0;
            vc.arrayUserInfo    = arrayUserInfo;
            vc.jobDetailsVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 101:{
            EducationMenuVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EducationMenuVC"];
            vc.arrayUserInfo     = arrayUserInfo;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;

        case 102:{
            ProfessionalDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfessionalDetailsVC"];
            vc.editMode         = NO;
            vc.updateSection    = 2;
            vc.arrayUserInfo    = arrayUserInfo;
            vc.professionalDetailsVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };

            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 103:{
            HobbiesVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HobbiesVC"];
            vc.editMode         = NO;
            vc.updateSection    = 3;
            vc.arrayUserInfo    = arrayUserInfo;
            vc.hobbiesVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };

            [self.navigationController pushViewController:vc animated:YES];

        }
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View Data Source & Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[arrayUserInfo objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * simpleTableIdentifier = @"CellIdentifierA";
    UITableViewCell * cell          = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.backgroundColor            = [UIColor clearColor];
    cell.selectionStyle             = UITableViewCellSelectionStyleNone;
    
    
    NSMutableDictionary * dictDetails   = [[arrayUserInfo objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    
    NSString * title    = nil;
    NSString * subTitle = nil;
    switch (indexPath.section) {
        case 0:{
            
            title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"title"],[dictDetails valueForKey:@"orgnization"]];
            
            
            //NSString   * tempString = [dictDetails valueForKey:@"skills"];
            //subTitle                = [tempString stringByReplacingOccurrencesOfString:@"," withString:@" "];
            
            subTitle = [NSString stringWithFormat:@"From %@ To %@\n",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
        }
    
            break;
        case 1:{
            
            //title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"school"],[dictDetails valueForKey:@"class"]];
            
            if([dictDetails valueForKey:@"school"])
                title       = [NSString stringWithFormat:@"\n%@",[dictDetails valueForKey:@"school"]];//[dictDetails valueForKey:@"class"]
            else
                title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"college"],[dictDetails valueForKey:@"degree"]];
            
            /*NSMutableString * stringTemp = [[NSMutableString alloc]init];
            if([[dictDetails valueForKey:@"skills"] isKindOfClass:[NSArray class]]){
                
                [[dictDetails valueForKey:@"skills"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    if(idx == 0)
                        [stringTemp appendString:[NSString stringWithFormat:@"%@",obj]];
                    else
                        [stringTemp appendString:[NSString stringWithFormat:@" %@",obj]];
                }];
            }else{
                [stringTemp appendString:[dictDetails valueForKey:@"skills"]];
            }
            subTitle    = stringTemp;*/
            subTitle = [NSString stringWithFormat:@"From %@ To %@\n",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
        }
            break;
        case 2:{
            title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"title"],[dictDetails valueForKey:@"orgnization"]];
            /*NSMutableString * stringTemp = [[NSMutableString alloc]init];
            if([[dictDetails valueForKey:@"skills"] isKindOfClass:[NSArray class]]){
                
                [[dictDetails valueForKey:@"skills"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if(idx == 0)
                        [stringTemp appendString:[NSString stringWithFormat:@"%@",obj]];
                    else
                        [stringTemp appendString:[NSString stringWithFormat:@" %@",obj]];

                }];
            }else{
                [stringTemp appendString:[dictDetails valueForKey:@"skills"]];
            }
            subTitle    = stringTemp;*/
            subTitle = [NSString stringWithFormat:@"From %@ To %@\n",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
            
        }
            break;
        case 3:
            title       = [dictDetails valueForKey:@"description"];
            //subTitle    = [dictDetails valueForKey:@"skills"];
            break;
            
        default:
            break;
    }
    
    cell.textLabel.numberOfLines        = 0;
    cell.detailTextLabel.numberOfLines  = 0;
    
    cell.textLabel.textColor            = [UIColor darkGrayColor];
    cell.detailTextLabel.textColor      = [UIColor darkGrayColor];
    
    cell.textLabel.font                 = [UIFont systemFontOfSize:14];
    cell.detailTextLabel.font           = [UIFont systemFontOfSize:13];

    cell.textLabel.text                 = title;
    cell.detailTextLabel.text           = subTitle;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSString * stringHeader = nil;
    
    switch (section) {
        case 0:
            stringHeader = @"Enter your job profile and Experience";
            break;
        case 1:
            stringHeader = @"Enter your Educational Qualifications";
            break;
        case 2:
            stringHeader = @"Enter your professional certificates";
            break;
        case 3:
            stringHeader = @"Enter your hobbies and co-curricular activities";
            break;
            
        default:
            break;
    }
    
    return [self becomeIPHeaderWithText:stringHeader tagValue:section];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    /*switch ([indexPath section]+100) {
            
        case 100:{
            JobDetailsVC * vc   = [self.storyboard instantiateViewControllerWithIdentifier:@"JobDetailsVC"];
            vc.editMode         = YES;
            vc.updateSection    = [indexPath section];
            vc.updateRow        = [indexPath row];
            vc.arrayUserInfo    = arrayUserInfo;
            vc.jobDetailsVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 101:{
            
            NSMutableDictionary * dictDetails   = [[arrayUserInfo objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
            
            if([[dictDetails valueForKey:@"detailsType"] isEqualToString:@"school"])
            {
                SchoolDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SchoolDetailsVC"];
                vc.arrayUserInfo     = arrayUserInfo;
                vc.editMode          = YES;
                vc.updateRow         = [indexPath row];
                vc.updateSection     = [indexPath section];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else{
                CollegeDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CollegeDetailsVC"];
                vc.arrayUserInfo     = arrayUserInfo;
                vc.editMode          = YES;
                vc.updateRow         = [indexPath row];
                vc.updateSection     = [indexPath section];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
            
        case 102:{
            ProfessionalDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfessionalDetailsVC"];
            vc.editMode         = YES;
            vc.updateSection    = [indexPath section];
            vc.updateRow        = [indexPath row];
            vc.arrayUserInfo    = arrayUserInfo;
            vc.professionalDetailsVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };

            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 103:{
            HobbiesVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"HobbiesVC"];
            vc.editMode         = YES;
            vc.updateSection    = [indexPath section];
            vc.updateRow        = [indexPath row];
            vc.arrayUserInfo    = arrayUserInfo;
            vc.hobbiesVCBlock = ^(){
                [tableViewIPProfile reloadData];
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        default:
            break;
    }*/
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 64;
}

-(BecomeIPHeader*)becomeIPHeaderWithText:(NSString*)textHeader tagValue:(NSInteger)value{
    
    
    BecomeIPHeader * becomeIPHeader     = [[[NSBundle mainBundle] loadNibNamed:@"BecomeIPHeader" owner:self options:nil] objectAtIndex:0];
    becomeIPHeader.backgroundColor      = [UIColor whiteColor];
    becomeIPHeader.labelTextHeder.text  = textHeader;
    becomeIPHeader.buttonAdd.tag        = 100+value;
    [becomeIPHeader.buttonAdd addTarget:self action:@selector(commonMethod:) forControlEvents:UIControlEventTouchUpInside];
    //NSInteger     viewWidth             = CGRectGetWidth(self.view.frame);
    //NSInteger     headerViewHeight      = viewWidth * 0.6;
    becomeIPHeader.frame                = CGRectMake(0, 8, CGRectGetWidth(tableViewIPProfile.frame), 48);
    return becomeIPHeader;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
