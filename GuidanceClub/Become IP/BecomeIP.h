//
//  BecomeIP.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/11/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"
#import "RevealController.h"
#import "ZUUIRevealController.h"

#import "BecomeIPHeader.h"

#import "SAMTextView.h"

#import "Utility.h"
#import "Constants.h"
#import "SVProgressHUD.h"

#import "JLTTagListView.h"


@interface BecomeIP : UIViewController < NSURLSessionDelegate>{
    
    __weak IBOutlet UITableView * tableViewIPProfile;
    NSMutableArray              * arrayUserInfo;
    
    __weak IBOutlet UITextField * textFieldRate;
    
    //__weak IBOutlet EYTagView * textViewPaidSkillTags;
    //__weak IBOutlet EYTagView * textViewFreeSkillTags;
    
    JLTTagListView   * textViewPaidSkillTags;
    JLTTagListView   * textViewFreeSkillTags;
    
}

@end
