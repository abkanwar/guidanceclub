//
//  HobbiesVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "HobbiesVC.h"

@interface HobbiesVC ()

@end

@implementation HobbiesVC
@synthesize arrayUserInfo, editMode, updateSection, updateRow;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Hobbies";
    imageViewBackTView.layer.borderColor    = [UIColor lightGrayColor].CGColor;
    textView.placeholder                    = @"Enter the keywords for which you want to provide the free consultation...";

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    if(editMode){
        
        NSMutableDictionary * dataDict = [[arrayUserInfo objectAtIndex:updateSection] objectAtIndex:updateRow];
        textView.text                  = [dataDict valueForKey:@"description"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)saveMethod:(id)sender{
    
    if(![self validateAllFields])
        return;
    
    NSMutableDictionary * arrayData = [[NSMutableDictionary alloc] init];
    [arrayData setValue:textView.text forKey:@"description"];
    
    //if(editMode)
    //    [[arrayUserInfo objectAtIndex:updateSection]replaceObjectAtIndex:updateRow withObject:arrayData];
    //else
    //    [[arrayUserInfo objectAtIndex:updateSection] addObject:arrayData];
    //self.hobbiesVCBlock();
    //[self.navigationController popViewControllerAnimated:YES];
    [self saveUserIntrestsOnServer];
}

-(void)saveUserIntrestsOnServer{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:STATUS_LOADING];
    });
    
    
    NSString * stringTags       = [textView text];
    NSString * stringTagsFinal  = [stringTags stringByReplacingOccurrencesOfString:@" " withString:@","];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, BECOME_IP];
    NSString * post             = [NSString stringWithFormat:@"user_type=IP&call_charges=0&skills=%@&user_id=%@&profile_id=is_ip", stringTagsFinal, [Utility userID]];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",responseDict);
        
        
        if(!responseDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
            return;
        }
        
        
        if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString    * meassage = @"User profile updated successfully.";
                [Utility showToastWithMessage:meassage target:self];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_IP"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationUserDetails" object:nil];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
            
        }
        
        else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 401){
            NSString    * meassage = @"Please specify a valid username or password.";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
    }];
    [postDataTask resume];
    
}

-(BOOL)validateAllFields{
    
    BOOL valid = YES;
    if(![textView.text length]){
        SHOW_ALERT(APP_NAME, @"Please enter keywords for free consultation", nil, @"OK", nil);
        valid = NO;
    }
    return valid;
}


- (void)setTitle:(NSString *)title {
    
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [titleLabel setLineBreakMode:NSLineBreakByClipping];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:COMMON_COLOR];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLabel setText:title];
    [titleLabel sizeToFit];
    [self navigationItem].titleView = titleLabel;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
