//
//  HobbiesVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Constants.h"
#import "SAMTextView.h"

#import "Utility.h"
#import "Constants.h"
#import "SVProgressHUD.h"

typedef void(^HobbiesVCBlock)();

@interface HobbiesVC : UIViewController <NSURLSessionDelegate>{
    
    __weak IBOutlet UIImageView     * imageViewBackTView;
    __weak IBOutlet SAMTextView     * textView;

}

@property (nonatomic, strong) NSMutableArray * arrayUserInfo;
@property (readwrite) BOOL                     editMode;
@property (readwrite) NSUInteger               updateSection;
@property (readwrite) NSUInteger               updateRow;

@property (nonatomic, strong) HobbiesVCBlock   hobbiesVCBlock;

@end
