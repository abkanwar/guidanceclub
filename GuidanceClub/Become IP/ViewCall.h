//
//  ViewCall.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/30/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MZTimerLabel.h>

@interface ViewCall : UIView{
    
}
@property (nonatomic, strong) IBOutlet UIButton      * buttonCall;
@property (nonatomic, strong) IBOutlet UIButton      * buttonSpeaker;
@property (nonatomic, strong) IBOutlet UIButton      * buttonMute;

@property (nonatomic, strong) IBOutlet UILabel       * labelCalling;
@property (nonatomic, strong) IBOutlet UILabel       * labelUserName;

@property (nonatomic, strong) IBOutlet UIImageView   * imageViewUser;

@property (nonatomic, strong) IBOutlet MZTimerLabel  * labelTimer;

@end
