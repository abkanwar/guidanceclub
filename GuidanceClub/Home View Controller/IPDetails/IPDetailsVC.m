//
//  IPDetailsVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/13/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "IPDetailsVC.h"

#define PADDING 10.f

@interface IPDetailsVC (){
    
    NSMutableDictionary * dictIPDetails;
}

@end

@implementation IPDetailsVC
@synthesize userId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayUserInfo = [[NSMutableArray alloc] init];
    for (int i = 0; i < 4; i++) {
        NSMutableArray * tempArray = [[NSMutableArray alloc] init];
        [arrayUserInfo addObject:tempArray];
    }
    
    tableViewIPDetails.estimatedRowHeight   = 44.0 ;
    tableViewIPDetails.rowHeight            = UITableViewAutomaticDimension;
    tableViewIPDetails.tableFooterView      = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self fetchUserDetailsFromServer:userId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchUserDetailsFromServer:(NSString*)userID{
    
    [SVProgressHUD showWithStatus:STATUS_LOADING];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, USER_PROFILE];
    NSString * post             = [NSString stringWithFormat:@"id=%@", userID];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * tempDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",tempDict);
        
        
        if(!tempDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
            return;
        }
        
        if([[tempDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                dictIPDetails = [tempDict valueForKey:@"response"];
                
                NSMutableArray * arrayJobs      = [tempDict valueForKeyPath:@"response.jobs"];
                if(arrayJobs !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:0 withObject:[arrayJobs mutableCopy]];
                
                NSMutableArray * arrayCollege   = [tempDict valueForKeyPath:@"response.colleges"];
                if(arrayCollege !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:1 withObject:[arrayCollege mutableCopy]];
                
                
                NSMutableArray * arraySchool      = [tempDict valueForKeyPath:@"response.schools"];
                if(arraySchool !=  (id)[NSNull null]){
                    [[arrayUserInfo objectAtIndex:1] addObjectsFromArray:[arraySchool mutableCopy]];
                }
                
                NSMutableArray * arraycertifications  = [tempDict valueForKeyPath:@"response.certifications"];
                if(arraycertifications !=  (id)[NSNull null])
                    [arrayUserInfo replaceObjectAtIndex:2 withObject:[arraycertifications mutableCopy]];
                
                NSLog(@"Jobs = %@", arrayJobs);
                [tableViewIPDetails reloadData];

                
                
            });
            
        }
        else if([[tempDict valueForKeyPath:@"status_code"] integerValue] == 401){
            NSString    * meassage = @"Message";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
    }];
    [postDataTask resume];
}


-(IBAction)commonMethod:(id)sender{
   
    if(![[SharedClass sharedInstance] isNetwork]){
        [Utility showToastWithMessage:@"Internet connection appears to be offline!" target:self];
        return;
    }
    
    if([sender tag] == 100){
        
        NSLog(@"Call By Phone Pressed");
        //NSDictionary * params = @{@"To": [dictIPDetails valueForKeyPath:@"avatar.name"]};
        //[dictIPDetails valueForKeyPath:@"avatar.name"]
        
         NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:[dictIPDetails valueForKey:@"mobile_no"], @"To", [dictIPDetails valueForKey:@"call_charges"],@"call_Charges", nil];
        [[APP_DELEGATE phone] connectWithParams:params];
        [APP_DELEGATE addCallViewToWindow:dictIPDetails];
        
    }else{
        
        NSLog(@"Call By App Pressed");
        //NSDictionary * params = @{@"To": [dictIPDetails valueForKeyPath:@"avatar.name"]};
        //[dictIPDetails valueForKeyPath:@"avatar.name"]
        
         NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:[dictIPDetails valueForKeyPath:@"avatar.name"], @"To", [dictIPDetails valueForKey:@"call_charges"],@"call_Charges", nil];
        [[APP_DELEGATE phone] connectWithParams:params];
        [APP_DELEGATE addCallViewToWindow:dictIPDetails];
    }
}

#pragma mark - Table View Data Source & Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   
    if(section == 0)
        return 1;
    else{
        NSLog(@"Count = %lu",(unsigned long)[[arrayUserInfo objectAtIndex:section-1] count]);
        return [[arrayUserInfo objectAtIndex:section-1] count];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([indexPath section] == 0){
        
        UITableViewCell * cell              = [self tableView:tableViewIPDetails cellForRowAtIndexPath:indexPath];
        UILabel         * labelDescription  = [cell viewWithTag:107];
        CGFloat cellHeight                  = CGRectGetMaxY(labelDescription.frame)+PADDING;
        return cellHeight;
    }
    else
        return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([indexPath section] == 0){
        
        static NSString * simpleTableIdentifier = @"CellIdentifierA";
        UITableViewCell * cell          = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.backgroundColor            = [UIColor whiteColor];
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
        
        
        
        UILabel * labelIPName           = [cell viewWithTag:100];
        UILabel * labelIPRate           = [cell viewWithTag:103];
        //UILabel * labelIPLocation       = [cell viewWithTag:104];
        UILabel * labelIPIntrestrs      = [cell viewWithTag:105];
        //UILabel * labelIPExperience     = [cell viewWithTag:106];
        UILabel * labelDescription      = [cell viewWithTag:107];
        
        labelIPName.text                = [dictIPDetails valueForKeyPath:@"avatar.name"];
        labelIPRate.text                = [NSString stringWithFormat:@"$ %@ per hour",[dictIPDetails valueForKey:@"call_charges"]];
        
        if(![[dictIPDetails valueForKey:@"interests"] isEqual:[NSNull null]] && [dictIPDetails valueForKey:@"interests"])
            labelIPIntrestrs.text       = [dictIPDetails valueForKey:@"interests"];
        
        
        if(![[dictIPDetails valueForKey:@"about"] isEqual:[NSNull null]] && [dictIPDetails valueForKey:@"about"])
            labelDescription.text           = [dictIPDetails valueForKey:@"about"];
        
        labelDescription.numberOfLines  = 0;
        CGRect oldFrame = labelDescription.frame;
        [labelDescription sizeToFit];
        CGRect newFrame = labelDescription.frame;
        newFrame.size.width = oldFrame.size.width;
        labelDescription.frame = newFrame;
        
        
        return cell;
        
    }else{
        
        static NSString * simpleTableIdentifier = @"CellIdentifierB";
        UITableViewCell * cell          = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.backgroundColor            = [UIColor clearColor];
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
        
        
        NSMutableDictionary * dictDetails   = [[arrayUserInfo objectAtIndex:[indexPath section]-1] objectAtIndex:[indexPath row]];
        
        NSString * title    = nil;
        NSString * subTitle = nil;
        switch (indexPath.section) {
            case 1:{
                
                title                   = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"title"],[dictDetails valueForKey:@"orgnization"]];
                //NSString   * tempString = [dictDetails valueForKey:@"skills"];
                //subTitle                = [tempString stringByReplacingOccurrencesOfString:@"," withString:@" "];
                subTitle = [NSString stringWithFormat:@"From %@ To %@  \n ",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
            }
                break;
                
            case 2:{
                
                if([dictDetails valueForKey:@"school"])
                    title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"school"],[dictDetails valueForKey:@"class"]];
                else
                    title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"college"],[dictDetails valueForKey:@"degree"]];
                
                /*NSMutableString * stringTemp = [[NSMutableString alloc]init];
                if([[dictDetails valueForKey:@"skills"] isKindOfClass:[NSArray class]]){
                    
                    [[dictDetails valueForKey:@"skills"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if(idx == 0)
                            [stringTemp appendString:[NSString stringWithFormat:@"%@",obj]];
                        else
                            [stringTemp appendString:[NSString stringWithFormat:@" %@",obj]];

                    }];
                }else{
                    [stringTemp appendString:[dictDetails valueForKey:@"skills"]];
                }
                subTitle    = stringTemp;*/
                subTitle = [NSString stringWithFormat:@"From %@ To %@\n",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
            }
                break;
                
            case 3:{
                
                title       = [NSString stringWithFormat:@"\n%@ (%@)",[dictDetails valueForKey:@"title"],[dictDetails valueForKey:@"orgnization"]];
                
                /*NSMutableString * stringTemp = [[NSMutableString alloc]init];
                if([[dictDetails valueForKey:@"skills"] isKindOfClass:[NSArray class]]){
                    
                    [[dictDetails valueForKey:@"skills"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        if(idx == 0)
                            [stringTemp appendString:[NSString stringWithFormat:@"%@",obj]];
                        else
                            [stringTemp appendString:[NSString stringWithFormat:@" %@",obj]];

                    }];
                }else{
                    [stringTemp appendString:[dictDetails valueForKey:@"skills"]];
                }
                subTitle    = stringTemp;*/
                subTitle = [NSString stringWithFormat:@"From %@ To %@\n",[dictDetails valueForKey:@"from"],[dictDetails valueForKey:@"to"]];
                
            }
                break;
                
            case 4:
                title       = [dictDetails valueForKey:@"description"];
                //subTitle    = [dictDetails valueForKey:@"skills"];
                break;
                
            default:
                break;
        }
        cell.textLabel.numberOfLines        = 0;
        cell.detailTextLabel.numberOfLines  = 0;
        
        cell.textLabel.textColor            = [UIColor darkGrayColor];
        cell.detailTextLabel.textColor      = [UIColor darkGrayColor];
        
        cell.textLabel.font                 = [UIFont systemFontOfSize:14];
        cell.detailTextLabel.font           = [UIFont systemFontOfSize:13];
        
        cell.textLabel.text                 = title;
        cell.detailTextLabel.text           = subTitle;
        
        return cell;


    }
    
    
}

- (UIView *)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableViewIPDetails.frame), 25)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView * imageView   = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(view.frame), 1)];
    imageView.backgroundColor = COMMON_COLOR;
    [view addSubview:imageView];
    
    UILabel * labelHeaderText = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(view.frame)-20, 25)];
    NSString *sectionName;
    switch (section){
        case 0:
            sectionName = @"";
            break;
        case 1:
            sectionName = @"Work Experience:-";
            break;
        case 2:
            sectionName = @"Education Details:-";
            break;
        case 3:
            sectionName = @"Professional Certificates:-";
            break;
        case 4:
            sectionName = @"Skills Set";
            break;
        case 5:
            sectionName = @"Free Consultaion:-";
            break;
        default:
            break;
    }
    labelHeaderText.text                = sectionName;
    [view addSubview:labelHeaderText];
    labelHeaderText.backgroundColor     = [UIColor clearColor];
    labelHeaderText.textColor           = COMMON_COLOR;
    [labelHeaderText setFont:[UIFont boldSystemFontOfSize:15.f]];
    
    return view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0)
        return 0;
    else{
        if([[arrayUserInfo objectAtIndex:section-1] count])
            return 25;
        else
            return 0;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
