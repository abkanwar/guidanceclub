//
//  IPDetailsVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/13/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utility.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@interface IPDetailsVC : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLSessionDelegate>{
    
    __weak IBOutlet UITableView * tableViewIPDetails;
    NSMutableArray              * arrayUserInfo;
}

@property (nonatomic, strong) NSString * userId;

@end
