//
//  ViewController.h
//  GuidanceClub
//
//  Created by Raman Kant on 12/15/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "RevealController.h"
#import "ZUUIRevealController.h"

@interface HomeViewController : UIViewController <UIAlertViewDelegate, UIActionSheetDelegate, ZUUIRevealControllerDelegate>{
    
    //__weak IBOutlet UITextField                 * textFieldUserName;
    //__weak IBOutlet UIButton                    * buttonLogin;
    //__weak IBOutlet UITextField                 * textFieldUserNameToCall;
    //__weak IBOutlet UIButton                    * buttonCall;
    //UIAlertView                                 * alertView;
    //__weak IBOutlet UISearchBar                 * searchBar;
}
//@property (nonatomic, strong)BasicPhone         * phone;


@end

