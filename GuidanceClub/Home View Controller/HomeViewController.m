//
//  ViewController.m
//  GuidanceClub
//
//  Created by Raman Kant on 12/15/15.
//  Copyright © 2015 Raman Kant. All rights reserved.
//

#import "HomeViewController.h"
#import "BasicPhoneNotifications.h"
#import "SearchDetailsVC.h"


@interface HomeViewController (){

    //TCDevice        * _phone;
    //TCConnection    * _connection;
}

/*
// Notifications //
-(void)loginDidStart:(NSNotification*)notification;
-(void)loginDidFinish:(NSNotification*)notification;
-(void)loginDidFailWithError:(NSNotification*)notification;

-(void)connectionDidConnect:(NSNotification*)notification;
-(void)connectionDidFailToConnect:(NSNotification*)notification;
-(void)connectionIsDisconnecting:(NSNotification*)notification;
-(void)connectionDidDisconnect:(NSNotification*)notification;
-(void)connectionDidFailWithError:(NSNotification*)notification;

-(void)pendingIncomingConnectionDidDisconnect:(NSNotification*)notification;
-(void)pendingIncomingConnectionReceived:(NSNotification*)notification;

-(void)deviceDidStartListeningForIncomingConnections:(NSNotification*)notification;
-(void)deviceDidStopListeningForIncomingConnections:(NSNotification*)notification;
// Notifications //
*/

@end

@implementation HomeViewController
//@synthesize phone;

#pragma mark - View & Memory Life Cycle -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    #if TARGET_IPHONE_SIMULATOR
    #else
    #endif
    /*
    NSString    * urlString = @"https://morning-tor-2589.herokuapp.com/token";
    NSURL       * url       = [NSURL URLWithString:urlString];
    NSError     * error     = nil;
    NSString    * capabilityToken     = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    if (capabilityToken == nil) {
        NSLog(@"Error retrieving token: %@", [error localizedDescription]);
        [[NSUserDefaults standardUserDefaults] setValue:capabilityToken forKey:CAPABILITY_TOKEN];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        NSLog(@"Error = %@", [error localizedDescription]);
    }
     */
    
    //NSString * capabilityToken = [Utility capabilityToken];
    
    //NSString    * urlString = [NSString stringWithFormat:@"https://morning-ridge-70693.herokuapp.com/token"];
    //NSURL       * url       = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    //NSURLResponse *response = nil;
    //NSError     * error     = nil;
    //NSData      * data      = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url]
    //                                                returningResponse:&response error:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
     self.title = @"Search";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithRed:211/255.0 green:94/255.0 blue:62/255.0 alpha:1.0f]}];
    
    //NSDictionary    * barAppearanceDict = @{NSFontAttributeName : [UIFont boldSystemFontOfSize:18.f], NSForegroundColorAttributeName: [UIColor whiteColor]};
    //[[UIBarButtonItem appearance] setTitleTextAttributes:barAppearanceDict forState:UIControlStateNormal];
    
    UIBarButtonItem * itemLeftA          = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuIcon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
    NSArray         * arrayLeftBatrItems = [[NSArray alloc]initWithObjects:itemLeftA, nil];
    self.navigationItem.leftBarButtonItems = arrayLeftBatrItems;
   
    //[[APP_DELEGATE  revealViewController] setDelegate:self];
    [APP_DELEGATE revealViewController].enableSwipeAndTapGestures = YES;
}

- (void)revealController:(ZUUIRevealController *)revealController willRevealRearViewController:(UIViewController *)rearViewController{
    
    UIView * view               = [[UIView alloc] initWithFrame:self.view.window.frame];
    view.backgroundColor        = [UIColor blackColor];
    view.tag                    = 555;
    view.userInteractionEnabled = YES;
    [self.view addSubview:view];
    view.alpha                  = 0.f;

    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        view.alpha                  = 0.35f;
    }
                     completion:^(BOOL finished) {
                         
                     }];
    //self.view.userInteractionEnabled = NO;
}
- (void)revealController:(ZUUIRevealController *)revealController willHideRearViewController:(UIViewController *)rearViewController{
    
    UIView * view = [self.view viewWithTag:555];
    [view removeFromSuperview];
    //self.view.userInteractionEnabled = YES;
}


-(void)viewDidUnload{
    
    // Unregister this class from all notifications
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidUnload];
}

-(void)dealloc{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
-(IBAction)login:(id)sender{
    
    NSString * userName = [textFieldUserName text];
    NSLog(@"User Name = %@", userName);
    [[APP_DELEGATE phone] login];
}

-(IBAction)call:(id)sender{
    
    NSString * userName = [textFieldUserNameToCall text];
    NSLog(@"User Name = %@", userName);
    [[APP_DELEGATE phone] login];
}*/

#pragma mark - SearchBar Delegate -

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    //[self.tableView reloadData];
    if (searchBar.text == nil) {
        return;
    }
    //[self searchText:searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    //[self searchText:searchBar.text];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    if (searchBar.text == nil) {
        return;
    }
    SearchDetailsVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDetailsVC"];
    vc.searchText        = searchBar.text;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    //[self.tableView reloadData];
    searchBar.text = nil;
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}


/*
#pragma mark - Set Up All Notifications -

-(void)setUpAllNotifications{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidStart:) name:BPLoginDidStart object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidFinish:) name:BPLoginDidFinish object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidFailWithError:) name:BPLoginDidFailWithError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionIsConnecting:) name:BPConnectionIsConnecting object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidConnect:) name:BPConnectionDidConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidDisconnect:) name:BPConnectionDidDisconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionIsDisconnecting:) name:BPConnectionIsDisconnecting object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidFailToConnect:) name:BPConnectionDidFailToConnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidFailWithError:) name:BPConnectionDidFailWithError object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingIncomingConnectionReceived:) name:BPPendingIncomingConnectionReceived object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pendingIncomingConnectionDidDisconnect:)  name:BPPendingIncomingConnectionDidDisconnect object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidStartListeningForIncomingConnections:) name:BPDeviceDidStartListeningForIncomingConnections object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidStopListeningForIncomingConnections:) name:BPDeviceDidStopListeningForIncomingConnections object:nil];
}

#pragma mark - Notifications Methods -

-(void)loginDidStart:(NSNotification*)notification{
    [self addStatusMessage:@"- Logging in -"];
}

-(void)loginDidFinish:(NSNotification*)notification{
    
    NSNumber * hasOutgoing = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityOutgoingKey];
    NSNumber * hasIncoming = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityIncomingKey];
    if ( [hasOutgoing boolValue] == YES ){
        [self addStatusMessage:@"- Outgoing calls allowed -"];
    }
    else{
        [self addStatusMessage:@"- Unable to make outgoing calls with current capabilities -"];
    }
    if ( [hasIncoming boolValue] == YES ){
        [self addStatusMessage:@"- Incoming calls allowed -"];
    }
    else{
        [self addStatusMessage:@"- Unable to receive incoming calls with current capabilities -"];
    }
    [self syncMainButton];
    [self updateCapabilitySignals];
}

-(void)loginDidFailWithError:(NSNotification*)notification{
    
    NSError* error = [[notification userInfo] objectForKey:@"error"];
    if ( error ){
        NSString* message = [NSString stringWithFormat:@"- Error logging in: %@ (%ld) -", [error localizedDescription], (long)[error code]];
        [self addStatusMessage:message];
    }
    else{
        [self addStatusMessage:@"- Unknown error logging in -"];
    }
    [self syncMainButton];
}

-(void)connectionIsConnecting:(NSNotification*)notification
{
    [self addStatusMessage:@"- Attempting to connect -"];
    [self syncMainButton];
}

-(void)connectionDidConnect:(NSNotification*)notification
{
    [self addStatusMessage:@"- Connection did connect -"];
    [self syncMainButton];
    
    //self.switchMuted.enabled = YES;
    //self.switchMuted.on = NO;
}

-(void)connectionDidFailToConnect:(NSNotification*)notification
{
    [self addStatusMessage:@"- Couldn't establish outgoing call -"];
    [self syncMainButton];
}

-(void)connectionIsDisconnecting:(NSNotification*)notification
{
    [self addStatusMessage:@"- Attempting to disconnect -"];
    [self syncMainButton];
}

-(void)connectionDidDisconnect:(NSNotification*)notification{
    [self addStatusMessage:@"- Connection did disconnect "];
    [self syncMainButton];
    //self.switchMuted.enabled = NO;
    //self.switchMuted.on = NO;
}

-(void)connectionDidFailWithError:(NSNotification*)notification{
    
    NSError * error = [[notification userInfo] objectForKey:@"error"];
    if ( error ){
        NSString* message = [NSString stringWithFormat:@"- Connection did fail with error code %ld, domain %@ -", (long)[error code], [error domain]];
        [self addStatusMessage:message];
    }
    [self syncMainButton];
}

-(void)deviceDidStartListeningForIncomingConnections:(NSNotification*)notification{
    [self addStatusMessage:@"- Device is listening for incoming connections -"];
}

-(void)deviceDidStopListeningForIncomingConnections:(NSNotification*)notification
{
    NSError* error = [[notification userInfo] objectForKey:@"error"]; // may be nil
    if ( error ){
        [self addStatusMessage:[NSString stringWithFormat:@"- Device is no longer listening for connections due to error: %@ (%ld) -", [error localizedDescription], (long)[error code]]];
    }
    else{
        [self addStatusMessage:@"- Device is no longer listening for connections -"];
    }
}


-(BOOL)isForeground{
    return [APP_DELEGATE isForeground];
}

-(void)pendingIncomingConnectionReceived:(NSNotification*)notification{
    
    NSDictionary* parameters = [notification userInfo];
    // Show alert view asking if user wants to accept or reject call //
    [self performSelectorOnMainThread:@selector(constructAlert:) withObject:parameters waitUntilDone:NO];
    // Check for background support
    if (![self isForeground]){
        // App is not in the foreground, so send LocalNotification //
        UIApplication       * app           = [UIApplication sharedApplication];
        UILocalNotification * notification  = [[UILocalNotification alloc] init];
        NSArray             * oldNots       = [app scheduledLocalNotifications];
        
        if ([oldNots count] > 0){
            [app cancelAllLocalNotifications];
        }
        notification.alertBody = @"Incoming Call";
        [app presentLocalNotificationNow:notification];
    }
    [self addStatusMessage:@"- Received incoming connection -"];
    [self syncMainButton];
}

-(void)pendingIncomingConnectionDidDisconnect:(NSNotification*)notification{
   
    // Make sure to cancel any pending notifications/alerts //
    [self performSelectorOnMainThread:@selector(cancelAlert) withObject:nil waitUntilDone:NO];
    if ( ![self isForeground] ){
        //App is not in the foreground, so kill the notification we posted. //
        UIApplication   * app = [UIApplication sharedApplication];
        [app cancelAllLocalNotifications];
    }
    [self addStatusMessage:@"- Pending connection did disconnect -"];
    [self syncMainButton];	
}

-(void)addStatusMessage:(NSString*)message{
    NSLog(@"Message = %@",message);
}


#pragma mark - UIAlertView -

-(void)constructAlert:(NSDictionary*)parameters{
    NSString* title = @"Incoming Call from ";
    title = [title stringByAppendingString:[parameters objectForKey:TCConnectionIncomingParameterFromKey]];
    alertView = [[UIAlertView alloc] initWithTitle:title
                                            message:nil
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"Accept",@"Reject", @"Ignore",nil];
    [alertView show];
}

-(void)cancelAlert{
    
    if(alertView){
        [alertView dismissWithClickedButtonIndex:1 animated:YES];
        alertView = nil;
    }
}


#pragma mark - UIAlertViewDelegate -

- (void)alertView:(UIAlertView* )alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
        //Accept button pressed
        if(!self.phone.connection){
            [self.phone acceptConnection];
        }
        else{
            //A connection already existed, so disconnect old connection and connect to current pending connectioon
            [self.phone disconnect];
            //Give the client time to reset itself, then accept connection
            [self.phone performSelector:@selector(acceptConnection) withObject:nil afterDelay:0.2];
        }
    }
    else if (buttonIndex == 1){
        // We don't release until after the delegate callback for connectionDidConnect:
        [self.phone rejectIncomingConnection];
    }
    else {
        [self.phone ignoreIncomingConnection];
    }
}


#pragma mark - Update UI -

-(void)syncMainButton{
    
    if ( ![NSThread isMainThread] ){
        [self performSelectorOnMainThread:@selector(syncMainButton) withObject:nil waitUntilDone:NO];
        return;
    }
    // Sync the main button according to the current connection's state //
    if (self.phone.connection) {
        
        if (self.phone.connection.state == TCConnectionStateDisconnected) {
            
            //Connection state is closed, show idle button
            [buttonLogin setImage:[UIImage imageNamed:@"idle"] forState:UIControlStateNormal];
        }
        
        else if (self.phone.connection.state == TCConnectionStateConnected) {
            
            //Connection state is open, show in progress button
            [buttonLogin setImage:[UIImage imageNamed:@"inprogress"] forState:UIControlStateNormal];
        }
        
        else if (self.phone.connection.state == TCConnectionStateConnecting) {
            
            [buttonLogin setImage:[UIImage imageNamed:@"predialing"] forState:UIControlStateNormal];
        }
        
        else {
            //Connection is in the middle of connecting. Show dialing button
            [buttonLogin setImage:[UIImage imageNamed:@"dialing"] forState:UIControlStateNormal];
        }
    }
    else {
        if (self.phone.pendingIncomingConnection) {
            
            //A pending incoming connection existed, show dialing button
            [buttonLogin setImage:[UIImage imageNamed:@"dialing"] forState:UIControlStateNormal];
        }
        else{
            
            //Both connection and _pending connnection do not exist, show idle button
            [buttonLogin setImage:[UIImage imageNamed:@"idle"] forState:UIControlStateNormal];
        }
    }
}
- (void)updateCapabilitySignals
{
    //NSNumber * hasIncoming = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityIncomingKey];
    //NSNumber * hasOutgoing = [self.phone.device.capabilities objectForKey:TCDeviceCapabilityOutgoingKey];
    //UIColor* colorRedLight = [UIColor colorWithRed:(CGFloat)(0xCC)/(255.0f) green:0.0f blue:0.0f alpha:1.0];
    //UIColor* colorGreenLight = [UIColor colorWithRed:(CGFloat)(0x33)/(255.0f) green:(CGFloat)(0xCC)/(255.0f) blue:0.0f alpha:1.0];
    //[self.viewIncomingSignal setBackgroundColor:([hasIncoming boolValue])? colorGreenLight : colorRedLight ];
    //[self.viewOutgoingSignal setBackgroundColor:([hasOutgoing boolValue])? colorGreenLight : colorRedLight ];
}

- (void)btnSelectOutgoingTypeClicked{
    
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Outgoing Type" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Number", @"Client", nil];
    [actionSheet showInView:self.view];
}
*/



@end
