//
//  SearchDetailsVC.m
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import "SearchDetailsVC.h"
#import "IPDetailsVC.h"

#define PADDING 60.f

@interface SearchDetailsVC (){
    
    CGFloat               cellHeight;
    NSMutableArray      * arrayList;
}

@end

@implementation SearchDetailsVC
@synthesize searchText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tableViewList.tableFooterView      = [[UIView alloc] initWithFrame:CGRectZero];
    [self searchIPWithKeyword:searchText];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchIPWithKeyword:(NSString*)keyword{
    
    [SVProgressHUD showWithStatus:STATUS_LOADING];
    
    NSString * urlString        = [NSString stringWithFormat:@"%@%@",BASE_URL, SEARCH];
    NSString * post             = [NSString stringWithFormat:@"keyword=%@", keyword];
    
    NSData              * requestData   = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request       = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration   * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession                * session       = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        
        if (error) {
            NSLog(@"Error:%@", [error localizedDescription]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:[error localizedDescription] target:self];
            });
            return;
        }
        
        NSError      * errorJson    = nil;
        NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&errorJson];
        NSLog(@"Response Dict = %@",responseDict);
        
        
        if(!responseDict && [errorJson code] == 3840){
            dispatch_async(dispatch_get_main_queue(), ^{
                //SHOW_ALERT(APP_NAME, [errorJson localizedDescription], nil, @"OK", nil);
                [Utility showToastWithMessage:[errorJson localizedDescription] target:self];
            });
            return;
        }
        
        if([[responseDict valueForKey:@"status_code"] integerValue] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([[responseDict valueForKeyPath:@"response.total_records"] intValue] == 0)
                    [Utility showToastWithMessage:@"No record found try with different keyword." target:self];
                else{
                    arrayList = [responseDict valueForKeyPath:@"response.result"];
                    [tableViewList reloadData];
                }
            });
            
        }
        else if([[responseDict valueForKeyPath:@"status_code"] integerValue] == 401){
            NSString    * meassage = @"Message";
            dispatch_async(dispatch_get_main_queue(), ^{
                [Utility showToastWithMessage:meassage target:self];
            });
        }
    }];
    [postDataTask resume];
}

#pragma mark - Table View Data Source & Delegate -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //UITableViewCell * cell      = [self tableView:tableViewList cellForRowAtIndexPath:indexPath];
    //UILabel * labelDescription  = [cell viewWithTag:111];
    //CGFloat cellHeight          = CGRectGetMaxY(labelDescription.frame)+PADDING;
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString * simpleTableIdentifier = @"CellIdentifier";
    UITableViewCell * cell      = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.backgroundColor        = [UIColor clearColor];
    cell.selectionStyle         = UITableViewCellSelectionStyleNone;
    
    
    NSMutableDictionary * dictIPDetails = [arrayList objectAtIndex:[indexPath row]];
    
    UILabel         * labelIPName       = [cell viewWithTag:100];
    UIImageView     * imageViewIPImage  = [cell viewWithTag:101];
    //UILabel         * labelUserLocation = [cell viewWithTag:103];
    //UILabel         * labelRating       = [cell viewWithTag:105];
    UILabel         * labelLastSeen     = [cell viewWithTag:108];
    //UILabel         * labelResponseRate = [cell viewWithTag:110];
    UILabel         * labelDescription  = [cell viewWithTag:111];
    UIButton        * buttonCallByPSTN  = [cell viewWithTag:112];
    UIButton        * buttonCallByApp   = [cell viewWithTag:113];
    UILabel         * labelCallCharges  = [cell viewWithTag:111];
    
    labelIPName.text        = [dictIPDetails valueForKeyPath:@"avatar.name"];
    imageViewIPImage.image  = [UIImage imageNamed:@"DummyUser"];//[dictIPDetails valueForKeyPath:@"avatar.image"]
    labelCallCharges.text   = [NSString stringWithFormat:@"$ %@ per hour",[dictIPDetails valueForKey:@"call_charges"]];
    labelLastSeen.text      = [Utility timeFromTimeStamp:[dictIPDetails valueForKeyPath:@"modified_on.sec"]];

    labelDescription.text   = @"User Description";
    CGRect oldFrame = labelDescription.frame;
    [labelDescription sizeToFit];
    CGRect newFrame = labelDescription.frame;
    newFrame.size.width = oldFrame.size.width;
    labelDescription.frame = newFrame;
    
    
    [buttonCallByPSTN addTarget:self action:@selector(callMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonCallByApp addTarget:self action:@selector(callMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    /*CGSize maximumLabelSize     = CGSizeMake(CGRectGetWidth(labelDescription.frame), FLT_MAX);
    CGSize expectedLabelSize    = [labelDescription.text sizeWithFont:labelDescription.font constrainedToSize:maximumLabelSize lineBreakMode:labelDescription.lineBreakMode];
    CGRect newFrame = labelDescription.frame;
    newFrame.size.height = expectedLabelSize.height;
    labelDescription.frame = newFrame;*/
    cellHeight          = CGRectGetMaxY(labelDescription.frame)+PADDING;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary * dictIPDetails = [arrayList objectAtIndex:[indexPath row]];
    IPDetailsVC         * vc            = [self.storyboard instantiateViewControllerWithIdentifier:@"IPDetailsVC"];
    vc.userId                           = [dictIPDetails valueForKeyPath:@"_id.$id"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)callMethod:(id)sender{
    
    
    if(![[SharedClass sharedInstance] isNetwork]){
        [Utility showToastWithMessage:@"The Internet connection appears to be offline !" target:self];
        return;
    }
    
    CGPoint           buttonPosition    = [sender convertPoint:CGPointZero toView:tableViewList];
    NSIndexPath     * indexPath         = [tableViewList indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index Path = %@",indexPath);

    NSMutableDictionary * dictIPDetails = [arrayList objectAtIndex:[indexPath row]];
    
    //[[SharedClass sharedInstance] setClientIDToCall:[dictIPDetails valueForKeyPath:@"avatar.name"]];
    if([sender tag] == 112){
       
        NSLog(@"Call To PSTN");
        //NSDictionary * params = @{@"To": [dictIPDetails valueForKeyPath:@"avatar.name"]}; //[dictIPDetails valueForKeyPath:@"avatar.name"]
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:[dictIPDetails valueForKey:@"mobile_no"], @"To",[dictIPDetails valueForKey:@"call_charges"],@"call_Charges", nil];
        
        [[APP_DELEGATE phone] connectWithParams:params];
        [APP_DELEGATE addCallViewToWindow:dictIPDetails];
        
    }else{
        
        NSLog(@"Call To App");
        //NSDictionary * params = @{@"To": [dictIPDetails valueForKeyPath:@"avatar.name"] }; //[dictIPDetails valueForKeyPath:@"avatar.name"]
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:[dictIPDetails valueForKeyPath:@"avatar.name"], @"To",[dictIPDetails valueForKey:@"call_charges"],@"call_Charges", nil];
        //[dictIPDetails valueForKeyPath:@"avatar.name"]
        [[APP_DELEGATE phone] connectWithParams:params];
        [APP_DELEGATE addCallViewToWindow:dictIPDetails];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
