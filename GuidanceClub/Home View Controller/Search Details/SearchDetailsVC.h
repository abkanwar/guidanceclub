//
//  SearchDetailsVC.h
//  GuidanceClub
//
//  Created by Raman Kant on 1/12/16.
//  Copyright © 2016 Raman Kant. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utility.h"
#import "Constants.h"
#import "SharedClass.h"
#import "SVProgressHUD.h"

@interface SearchDetailsVC : UIViewController < UITableViewDataSource, UITableViewDelegate, NSURLSessionDelegate>{
    
    __weak IBOutlet UITableView         * tableViewList;
    __weak IBOutlet UISearchBar         * searchBar;
}
@property (nonatomic, strong) NSString  * searchText;

@end
